<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class IconPickerAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/fontawesome-iconpicker/css/fontawesome-iconpicker.min.css',
    ];
    public $js = [
        'plugins/fontawesome-iconpicker/js/fontawesome-iconpicker.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
      //  'digitv\bootstrap\assets\BootstrapAsset',
       // 'digitv\bootstrap\assets\BootstrapPluginAsset',
    ];

}
