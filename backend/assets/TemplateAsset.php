<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class TemplateAsset extends AssetBundle {

    public function init() {
        parent::init();
        $this->css[] = 'template/assets/skins/' . Yii::$app->params['tema'] . '.min.css'; // dynamic file added
        $this->css[] = 'css/site.css';
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'plugins/cropper/cropper.min.css',

        /* 'global/css/bootstrap.min.css',
          'global/css/bootstrap-extend.min.css', */

        'template/assets/css/site.min.css',
        'global/vendor/animsition/animsition.css',
        'global/vendor/asscrollable/asScrollable.css',
        'global/vendor/switchery/switchery.css',
        //'global/vendor/intro-js/introjs.css',
        'global/vendor/slidepanel/slidePanel.css',
        'global/vendor/flag-icon-css/flag-icon.css',
        //'global/vendor/waves/waves.css',
        //'global/vendor/chartist/chartist.css',
        //'global/vendor/jvectormap/jquery-jvectormap.css',
        //'global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css',
        //'template/assets/examples/css/dashboard/v1.css', //BORRAR
        'global/fonts/material-design/material-design.min.css',
        'global/fonts/brand-icons/brand-icons.min.css',
        'global/fonts/font-awesome/font-awesome.min.css',
        'global/fonts/glyphicons/glyphicons.min.css',
        'http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic',
    ];
    public $js = [
        //'plugins/cropper/cropper.min.js',
        'global/vendor/babel-external-helpers/babel-external-helpers.js',
        //'global/vendor/jquery/jquery.js',
        //'global/vendor/tether/tether.js',
        //'global/vendor/bootstrap/bootstrap.js',
        'global/vendor/animsition/animsition.js',
        'global/vendor/mousewheel/jquery.mousewheel.js',
        'global/vendor/asscrollbar/jquery-asScrollbar.js',
        'global/vendor/asscrollable/jquery-asScrollable.js',
        'global/vendor/ashoverscroll/jquery-asHoverScroll.js',
        //'global/vendor/waves/waves.js',
        //'global/vendor/switchery/switchery.min.js',
        //'global/vendor/intro-js/intro.js',
        'global/vendor/screenfull/screenfull.js',
        'global/vendor/slidepanel/jquery-slidePanel.js',
        //'global/vendor/chartist/chartist.min.js',
        //'global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js',
        //'global/vendor/jvectormap/jquery-jvectormap.min.js',
        //'global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js',
        //'global/vendor/matchheight/jquery.matchHeight-min.js',
        //'global/vendor/peity/jquery.peity.min.js',
        'global/js/State.js',
        'global/js/Component.js',
        'global/js/Plugin.js',
        'global/js/Base.js',
        'global/js/Config.js',
        'template/assets/js/Section/Menubar.js',
        'template/assets/js/Section/GridMenu.js',
        'template/assets/js/Section/Sidebar.js',
        'template/assets/js/Section/PageAside.js',
        'template/assets/js/Plugin/menu.js',
        'global/js/config/colors.js',
        'template/assets/js/config/tour.js',
        'template/assets/js/config/assets.js',
        'template/assets/js/Site.js',
        'global/js/Plugin/asscrollable.js',
        'global/js/Plugin/slidepanel.js',
        'global/js/Plugin/switchery.js',
        'global/js/Plugin/matchheight.js',
        //'global/js/Plugin/jvectormap.js',
        'global/js/Plugin/peity.js',
        'template/assets/js/Run.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'backend\assets\CoreAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        //'digitv\bootstrap\assets\BootstrapAsset',
        //'digitv\bootstrap\assets\BootstrapPluginAsset',
            /* 'yii\web\YiiAsset',
              'yii\web\JqueryAsset',
             */
    ];

}
