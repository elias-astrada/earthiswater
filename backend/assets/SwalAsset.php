<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class SwalAsset extends AssetBundle
{


    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/swal2/sweetalert2.min.css',
    ];
    public $js = [
        'plugins/swal2/sweetalert2.min.js',
    ];
    public $depends = [
                'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        /*        'digitv\bootstrap\assets\BootstrapAsset',
        'digitv\bootstrap\assets\BootstrapPluginAsset',*/
    ];
}
