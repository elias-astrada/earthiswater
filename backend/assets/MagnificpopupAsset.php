<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class MagnificpopupAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'global/vendor/magnific-popup/magnific-popup.css',
    ];
    public $js = [
        'global/vendor/magnific-popup/jquery.magnific-popup.js',
        'global/js/Plugin/magnific-popup.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
            //'digitv\bootstrap\assets\BootstrapAsset',
            //'digitv\bootstrap\assets\BootstrapPluginAsset',
    ];

}
