<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class FancyboxAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        'plugins/fancybox/jquery.fancybox.min.css',
        
    ];
    public $js = [
 
        'plugins/fancybox/jquery.fancybox.min.js',
        
    ];
    
    
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        
    ];

}
