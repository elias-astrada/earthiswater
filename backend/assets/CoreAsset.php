<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class CoreAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'plugins/cropper/cropper.min.css',
        
       /* 'global/css/bootstrap.min.css',
        'global/css/bootstrap-extend.min.css',*/
        
    ];
    public $js = [
        // 'plugins/cropper/cropper.min.js',
       // 'global/vendor/babel-external-helpers/babel-external-helpers.js',
       /* 'global/vendor/jquery/jquery.js',
        'global/vendor/tether/tether.js',
        'global/vendor/bootstrap/bootstrap.js',
        */
      
        
    ];
    
    
    public $depends = [
       // 'yii\web\YiiAsset',
        /*'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',*/
    ];

}
