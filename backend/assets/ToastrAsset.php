<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class ToastrAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'global/vendor/toastr/toastr.css',
        'template/assets/examples/css/advanced/toastr.css'
    ];
    public $js = [
        'global/vendor/toastr/toastr.js',
        'template/assets/js/config/tour.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        //'digitv\bootstrap\assets\BootstrapAsset',
       // 'digitv\bootstrap\assets\BootstrapPluginAsset',
    ];

}
