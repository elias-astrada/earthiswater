<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class TetherAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        
    ];
    public $js = [

        'global/vendor/tether/tether.js',

        
      
        
    ];
    
    
    public $depends = [
        //'yii\web\YiiAsset',
        /*'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',*/
    ];

}
