<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public function init() {
        parent::init();
        $this->css[] = 'css/themes/' . Yii::$app->params['tema'] . '.min.css'; // dynamic file added
        $this->css[] = 'css/custom.css';
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'plugins/font-awesome/css/font-awesome.min.css',
        'plugins/simple-line-icons/simple-line-icons.min.css',
        /* 'plugins/bootstrap-switch/css/bootstrap-switch.min.css',
          'plugins/jquery-filer/css/jquery.filer.css',
          'plugins/jquery-filer/css/themes/jquery.filer-dragdropbox-theme.css',
          'plugins/bootstrap-toastr/toastr.min.css',

          'plugins/jquery-multi-select/css/multi-select.css',
          'plugins/datatables/datatables.min.css',
          'plugins/datatables/plugins/bootstrap/datatables.bootstrap.css', */
        'css/components.min.css',
        'plugins/fancybox/jquery.fancybox.min.css',
        'css/plugins.min.css',
        'css/fontawesome-iconpicker.min.css',
        'css/layout.min.css',
    ];
    public $js = [
        // 'plugins/jquery.min.js',
        // 'plugins/bootstrap/js/bootstrap.min.js',
        /*



          'plugins/moment.min.js',
          'plugins/bootstrap-daterangepicker/daterangepicker.min.js',
          'plugins/morris/morris.min.js',
          'plugins/morris/raphael-min.js',
          'plugins/bootstrap-toastr/toastr.min.js',
          'plugins/jquery-filer/js/jquery.filer.min.js',
          'plugins/fancybox/source/jquery.fancybox.js',
          'plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
          'plugins/bootbox/bootbox.min.js',
          'scripts/datatable.js',
          'plugins/datatables/datatables.min.js',
          'plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
          'scripts/ajaxupload.min.js',
          'scripts/table-datatables-editable.js',
          'scripts/dashboard.min.js', */


        'plugins/jquery.blockui.min.js',
        'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'plugins/fancybox/jquery.fancybox.min.js',
        'plugins/js.cookie.min.js',
        'js/app.min.js',
        'js/quick-sidebar.min.js',
        'js/fontawesome-iconpicker.min.js',
        //'js/demo.min.js',
        'js/layout.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        //'digitv\bootstrap\assets\BootstrapAsset',
        //'digitv\bootstrap\assets\BootstrapPluginAsset',
    ];

}
