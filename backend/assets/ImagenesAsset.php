<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Main backend application asset bundle.
 */
class ImagenesAsset extends AssetBundle
{


    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'template/assets/examples/css/apps/media.css'
    ];

    public $js = [
        'template/assets/js/App/animate-list.js',
       /* 'template/assets/js/App/action-btn.js',
        'template/assets/js/App/asselectable.js',
        'template/assets/js/App/selectable.js',*/
        
        'template/assets/js/BaseApp.js',
        'template/assets/js/App/Media.js',
        'template/assets/examples/js/apps/media.js',
    ];
   public $depends = [
        'backend\assets\TemplateAsset',

    ];
}
