<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);


return [
    'id' => 'app-backend',
    'name' => $params['webname'].'-Admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'layout' => 'remark',
    //$params['layout']
    'modules' => [
        'user' => [
            'controllerMap' => [
                'admin' => 'backend\controllers\users\AdminController',
                'profile' => 'backend\controllers\users\ProfileController',
                'recovery' => 'backend\controllers\users\RecoveryController',
                'registration' => 'backend\controllers\users\RegistrationController',
                'security' => 'backend\controllers\users\SecurityController',
                'settings' => 'backend\controllers\users\SettingsController',
            ],
        ],
        'permisos' => [
            'class' => 'mdm\admin\Module',
            'viewPath' => '@app/views/permisos',
            'defaultRoute' => 'default',
            'controllerMap' => [
                'assignment' => 'backend\controllers\permisos\AssignmentController',
                //'user' => 'backend\controllers\permisos\UserController',
                //'default' => 'backend\controllers\permisos\DefaultController',
                'menu' => 'backend\controllers\permisos\MenuController',
                //'permission' => 'backend\controllers\permisos\PermissionController',
                //'role' => 'backend\controllers\permisos\RoleController',
               // 'rule' => 'backend\controllers\permisos\RuleController',
               // 'route' => q   'backend\controllers\permisos\RouteController',
            ],
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
    ],
    'components' => [
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => 'global/vendor/jquery/',
                    //'jsOptions' => ['position' => 1],
                    'js' => ['jquery.js']
                   // 'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => 'global/css',
                    'css' => ['bootstrap.min.css', 'bootstrap-extend.min.css'],
                    //'css' => [],
                    //'js' => ['vendor/tether/tether.js','vendor/bootstrap/bootstrap.js',]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => 'global/vendor/bootstrap',
                    'js' => ['bootstrap.js',],
                    'depends' => ['yii\web\JqueryAsset','backend\assets\TetherAsset']
                ],
                
                'kartik\dialog\DialogBootstrapAsset' => [
                    'sourcePath' => 'global',
                    'css' => ['css/bootstrap-dialog.css'],
                    'js' => ['js/bootstrap-dialog.js'],
                    'depends' => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapAsset',
                        'yii\bootstrap\BootstrapPluginAsset',
                        
                    ]
                    
                ]
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => $params['ruta_base'] . 'backend/',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => $params['ruta_base'] . 'backend/',
            'rules' => [
                '<action:\w+>' => 'site/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'urlManagerFront' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => $params['ruta_base'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => ['site/error', 'site/logout', 'user/security/*', 'user/registration/*', 'user/recovery/*', 'debug/*'],
    // The actions listed here will be allowed to everyone including guests.
    // So, 'admin/*' should not appear here in the production, of course.
    // But in the earlier stages of your development, you may probably want to
    // add a lot of actions here until you finally completed setting up rbac,
    // otherwise you may not even take a first step.
    ],
    'params' => $params,
];
