<?php

use kartik\grid\GridView;

$defaultExportConfig = [
    GridView::HTML => [],
    GridView::CSV => [],
    GridView::TEXT => [],
    GridView::EXCEL => [],
    /* GridView::PDF => [], */
    GridView::JSON => [],
];

return [
    'adminEmail' => 'admin@example.com',
    //'proyecto' => $params['webname'],
    'showUserImage' => true,
    'showPageToolBar' => false,
    /* LAYOUT */
    'tema' => 'indigo', /* blue, brown, cyan, green, grey, indigo,orange,pink,purple,red,teal, yellow */



    /* OPCIONES GRIDVIEW GRAL */
    'gridview_exportConfig' => $defaultExportConfig,
    'nav-search-show' => false,
    'page-header-show' => true,
    'page-header-showtitle' => true,
    'page-content-show' => true,
    'page-content-class' => 'container-fluid',
    'page-body-class' => 'dashboard',
    'recortes' => [
        '16/9x2' => '1280x720',
        '16/9x1' => '640x360',
        '4/3x1' => '640x480',
        'landscape' => '1280x720'
    ],
    
    
    /* CONFIGURACION VIEJA  */
    'layout' => 'main', /* main  */
    'tema_login' => '1', /* 1 */
    'showSidebar' => false,
    'showNotificaciones' => true,
    'showMessages' => true,
    'showTasks' => false,
];



