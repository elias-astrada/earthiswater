<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\UserMessages;
use common\models\search\UserMessagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\db\Query;

/**
 * UserMessagesController implements the CRUD actions for UserMessages model.
 */
class UserMessagesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index', 'send','view', 'create', 'delete', 'delete-multiple', 'userlist'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all UserMessages models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserMessagesSearch();
        
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,\Yii::$app->user->id);
        
        $inbox_n = UserMessages::find()->where(['visto' => 0])->count();
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            'inbox_n' => $inbox_n,
            'send_active' => false,
        ]);
    }
    
    
    public function actionSend(){
        $searchModel = new UserMessagesSearch();
        
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,null,\Yii::$app->user->id);
        
        $inbox_n = UserMessages::find()->where(['visto' => 0])->count();
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'inbox_n' => $inbox_n,
            'send_active' => true,
        ]); 
    }

    /**
     * Displays a single UserMessages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $model = $this->findModel($id);

        if ($model->user_to != Yii::$app->user->id) {
            throw new \yii\web\BadRequestHttpException('No puede ver mensajes de otros usuarios.');
        }

        if (!$model->isVisto()) {
            $model->setVisto();
        }



        if (Yii::$app->request->isAjax)
            return $this->renderPartial('view_modal', [
                        'model' => $model,
            ]);
        else
            return $this->render('view', [
                        'model' => $model,
            ]);
    }

    /**
     * Creates a new UserMessages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($to = null) {
        $model = new UserMessages();

        if (!is_null($to)) {
            $model->user_to = $to;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->fecha = time();
            $model->visto = 0;
            $model->user_from = Yii::$app->user->id;

            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'El mensaje ha sido enviado exitosamente.');
                return $this->redirect(['index']);
            } else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        }


        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUserlist($q = null, $id = null) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, username AS text')
                    ->from('user')
                    ->where(['like', 'username', $q])
                    ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => \common\models\User::find($id)->username];
        }
        return $out;
    }

    public function actionDeleteMultiple() {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        return UserMessages::deleteAll(['id' => $pk]);
    }

    /**
     * Deletes an existing UserMessages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {


        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            return Json::encode([
                        'success' => true,
            ]);
        } else
            return $this->redirect(['index']);
    }

    /**
     * Finds the UserMessages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserMessages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UserMessages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

}
