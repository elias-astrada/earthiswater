<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\UserNotifications;
use common\models\search\UserNotifications as UserNotificationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * NotificacionesController implements the CRUD actions for UserNotifications model.
 */
class NotificacionesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'visto' => ['POST'],
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','delete','visto','delete-multiple'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all UserNotifications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserNotificationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionVisto(){

        if (!Yii::$app->request->isAjax)  return $this->redirect(['index']);

        $ids = Yii::$app->request->post('notificaciones');

        if($ids == null || $ids == '') return \yii\helpers\Json::encode('Notificaciones NULL');

        foreach($ids as $id){
           $m = UserNotifications::findOne($id);
           $m->visto = 1;
           $m->save(false);
        }

        return \yii\helpers\Json::encode('OK');

    }
    
    /**
     * Displays a single UserNotifications model.
     * @param integer $id
     * @return mixed
     */
     
    public function actionView($id)
    {   

        $model = $this->findModel($id);

        return $this->renderPartial('view', [
            'model' => $model,
        ]);
    }




    public function actionDeleteMultiple(){
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        return UserNotifications::deleteAll(['id' => $pk]);
    }


    /**
     * Deletes an existing UserNotifications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {


        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax ) {
            return Json::encode([
                'success' => true,
            ]);
        }
        else return $this->redirect(['index']);
    }

    /**
     * Finds the UserNotifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserNotifications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserNotifications::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }
}
