<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\Productos;
use common\models\Archivos;
use common\models\search\ProductosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * ProductosController implements the CRUD actions for Productos model.
 */
class ProductosController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple', 'multimedia-remove'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Productos models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Productos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $model = $this->findModel($id);

        return $this->renderPartial('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new Productos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Productos();

        if ($model->load(Yii::$app->request->post())) {

            $model->fecha_creacion = time();
            $model->fecha_modificacion = time();




            $multimedia_uploaded = Yii::$app->request->post('multimedia-ids');
            if (!is_null(Yii::$app->request->post('palabras_clave')))
                $model->keywords = implode(',', Yii::$app->request->post('palabras_clave'));




            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido guardados exitosamente.');


                $relation_error = false;
                if (!empty($multimedia_uploaded)) {
                    $relation_error = $this->relacionarMultimedia($multimedia_uploaded, $model);
                }

                if ($relation_error) {
                    \Yii::$app->getSession()->setFlash('warning', 'Hubo un problema al relacionar los archivos multimedia cargados. Sin embargo los datos de la nota han sido guardados correctamente.');
                }


                return $this->redirect(['index']);
            } else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        }


        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    //Archivos list string con ids separados con coma "1,2,3,4"
    protected function relacionarMultimedia($archivoslist, $model) {
        $relation_error = false;
        foreach (explode(',', $archivoslist) as $f_id) {


            $f_h = \common\utils\Archivo::loaddb($f_id);
            $archivo = $f_h->getDbRecord();
            try{
                $model->link('archivosMultimedia', $archivo);
            } catch (Exception $ex) {
                $relation_error = true;
            }
        }


        return $relation_error;
    }

    public function actionMultimediaRemove($id) {

        //implementar
        $model = $this->findModel($id);

        $file_id = Yii::$app->request->post("multimedia_id");

        $archivo = Archivos::findOne($file_id);

        if (is_null($archivo))
            return true;


        $model->unlink('archivosMultimedia', $archivo, true);
    }

    public function actionDeleteMultiple() {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        return Productos::deleteAll(['id' => $pk]);
    }

    /**
     * Updates an existing Productos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {


            $multimedia_uploaded = Yii::$app->request->post('multimedia-ids');
            $model->fecha_modificacion = time();

            if (!is_null(Yii::$app->request->post('palabras_clave')))
                $model->keywords = implode(',', Yii::$app->request->post('palabras_clave'));



            $relation_error = false;
            if (!empty($multimedia_uploaded)) {
                $relation_error = $this->relacionarMultimedia($multimedia_uploaded, $model);
            }

            if ($relation_error) {
                \Yii::$app->getSession()->setFlash('warning', 'Hubo un problema al relacionar los archivos multimedia cargados. Sin embargo los datos de la nota han sido guardados correctamente.');
            }


            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido modificados exitosamente.');
                return $this->redirect(['index']);
            } else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        }


        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Productos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {


        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            return Json::encode([
                        'success' => true,
            ]);
        } else
            return $this->redirect(['index']);
    }

    /**
     * Finds the Productos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Productos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Productos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

}
