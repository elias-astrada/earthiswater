<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Url;

/*
  $input = Html::activeInput('hidden', $this->model, $this->attribute, ['class' => 'imgpanel-' . $this->attribute]);
 */
?> 


<div id="multimedia-container-widget-<?= $wid ?>">
    <input type="hidden" id="multimedia-<?= $attribute ?>-ids" name="multimedia-ids">
    <?= ""/* Html::activeInput('hidden', $this->model, $this->attribute, ['class' => 'multimedaw-' . $this->attribute]); */ ?>


    <div class="row multimedia-list-<?= $attribute ?>">

        <?php foreach ($dataprovider_multimedia as $multimedia) : ?>

            <div class="col-3 multimedia-list-item" data-multimedia-id = "<?= $multimedia->id ?>" >
                <figure class="overlay overlay-hover">
                    <img class="img-fluid" src="<?= $multimedia->tipo == "youtube" ? "https://img.youtube.com/vi/".$multimedia->extension."/0.jpg"  : $multimedia->getUrl() ?>" alt="...">
                    <figcaption class="overlay-panel overlay-background overlay-slide-left overlay-icon">
                        <a class="icon md-delete multimedia-delete-button-<?= $wid ?>" href="#search" ></a>
                        <a class="icon md-crop multimedia-crop-button remote-modal" target = '_BLANK'  data-target = "#modal-multimedia-recortes"  data-toggle = "modal" href="<?= Url::toRoute(['/multimedia/recortar', 'id' => $multimedia->id]) ?>"></a>
                        <a class="icon md-search multimedia-zoom-button" data-fancybox="gallery" href="<?= $multimedia->getUrl() ?>" ></a>
                    </figcaption>
                </figure> 
            </div>

        <?php endforeach; ?>


        <div class="col-3">
            <button type="button" data-toggle="modal" data-target="#modal-multimedia-select-<?= $wid ?>" class="btn btn-floating btn-info waves-effect waves-classic"><i class="icon md-plus" aria-hidden="true"></i></button>
        </div>

    </div>


    <?= "" /*$this->render("multimediaModals", compact('dataProvider_galeria', 'wid')) */ ?>

</div>

<?php
$removemultimediaurl = Url::toRoute(['/multimedia/delete']);
$cropper_url = Url::toRoute(['/multimedia/recortar']);




$this->registerJs(
        <<<JAVASCRIPT
        
        
    $('a.md-search').magnificPopup({
      type: 'image'
      // other options
    });
        
    $(document).on("click", ".multimedia-delete-button-$wid", function() {
        
        var figureparent = $(this).closest('.multimedia-list-item');
        var parent_data = figureparent.data();
        var id_multimedia = parent_data.multimediaId;

        remove_multimedia_$wid(id_multimedia , figureparent);
    });
        
        
    function select_multimedia_$wid(url_img , archivo_id ,tipo  ){
        
        
        
        var prepend_html = '<div class="col-3 multimedia-list-item" data-multimedia-id = "'+archivo_id+'" ><figure class="overlay overlay-hover"><img class="img-fluid" src="'+url_img+'" alt="..."><figcaption class="overlay-panel overlay-background overlay-slide-left overlay-icon"><a class="icon md-delete  multimedia-delete-button-$wid" href="#search" ></a><a class="icon md-crop remote-modal multimedia-crop-button" target = "_BLANK"  data-target = "#modal-multimedia-recortes"  data-toggle = "modal" href="$cropper_url?id='+archivo_id+'"></a> <a class="icon md-search multimedia-zoom-button" href="'+url_img+'"  ></a></figcaption></figure> </div>';
        
        
        switch(tipo){
            
            case 'vid':
                $("#multimedia-container-widget-$wid").find(".multimedia-list-$attribute").prepend(prepend_html );
            break;
            case 'img': default:
                $("#multimedia-container-widget-$wid").find(".multimedia-list-$attribute").prepend(prepend_html );
            break;
        
        }
        
        
        register_on_input_$wid(archivo_id);
            $('a.md-search').magnificPopup({
            type: 'image'
            // other options
          });
        
    }
        
        
    function remove_multimedia_$wid(archivo_id,jquery_object){
        
        
        jquery_object.remove();
        unregister_on_input_$wid(archivo_id);
        
        
       $.ajax({
            type: "POST",
            url: "$unlink_url",
            data: {multimedia_id : archivo_id},

            success: function(data) {


        
            },

        });
        
    }

    function register_on_input_$wid(multimedia_id){
        var inputvalue = $("#multimedia-$attribute-ids").val();
        if(inputvalue == ""){
             $("#multimedia-$attribute-ids").val(multimedia_id);
        }else{
            var filesarray = inputvalue.split(",");
            filesarray.push(multimedia_id);
            $("#multimedia-$attribute-ids").val(filesarray.join(','));
        }

    }  
        
        
    function unregister_on_input_$wid(multimedia_id){
        var inputvalue = $("#multimedia-$attribute-ids").val();
        var filesarray = inputvalue.split(",");
        var id_search = multimedia_id;


        for (var i=filesarray.length-1; i>=0; i--) {
        
            if (filesarray[i] == id_search) {
                filesarray.splice(i, 1);
                break;    
            }
        }
        
       
        $("#multimedia-$attribute-ids").val(filesarray.join(','));
    }

        
JAVASCRIPT
);
