

$('.advanced_search_btn').click(function(event) {
	display_advanced_search();
});

function display_advanced_search(){
	$('.advanced_search_form').toggle('fast');
}

function multidelete(ids, route){
	if (ids != '' && route != ''){
		$.ajax({
			url: route,
			type: 'POST',
			dataType: 'JSON',
			data: {id: ids},
		})
		.done(function(data) {
			if (data.success)
				$.pjax.reload({container:'#grid_index'});
		})	
	}
}


$(document).on('click', '.ajax-call', function(e){ 
    //console.log("DEBUG ALPHA");
    e.preventDefault();
    url = $(this).data("url");
    method = $(this).data("method");
    params = $(this).data("params");
    button = $(this);

    $.ajax({
        type: method,
        url: url,
        data: params,
        success: function(result) {
            console.log(result);
            button.trigger( "ajax-call-response" ,[result]);
        },
        error: function(result) {
            button.trigger( "ajax-call-error" ,[result]);
        }
    });
});






$(document).on('click', '.remote-modal', function(e){ 
  e.preventDefault();
  
//  console.log("debutg");
  $($(this).data('target')).modal('show').find('.modal-content').load($(this).attr('href'));
});
