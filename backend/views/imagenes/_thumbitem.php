<?php

use yii\helpers\Html;
use yii\helpers\Url;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * data-toggle="slidePanel" data-url="<?= Url::toRoute(['/imagenes/recortar', 'id' => $model->id]) ?>"
 */
?>



<div class="media-item" >
    <!--<div class="checkbox-custom checkbox-primary checkbox-lg">
        <input type="checkbox" class="selectable-item" id="media_12" />
        <label for="media_12"></label>
    </div>-->
    <div class="image-wrap">
        <img src="<?= $model->getUrl() ?>" class="image img-rounded" alt="...">
    </div>
    <div class="info-wrap">
        <div class="dropdown">
            <span class="icon md-settings" data-toggle="dropdown" aria-expanded="false" role="button"
                  data-animation="scale-up"></span>
            <div class="dropdown-menu dropdown-menu-right" role="menu">
                <?= Html::a('<i class="icon md-edit" aria-hidden="true"></i>Recortar', ['/imagenes/recortar', 'id' => $model->id], ['class' => 'dropdown-item remote-modal', 'target' => '_BLANK', 'data-target' => '#modal-recortes', 'data-toggle' => 'modal']);
                ?>
                <?= "" /* Html::a('<i class="icon md-download" aria-hidden="true"></i>Descargar', ['/imagenes/recortar', 'id' => $model->id], ['class' => 'dropdown-item remote-modal', 'target' => '_BLANK', 'data-target' => '#modal-recortes', 'data-toggle' => 'modal']);
                 */ ?>
                <?= Html::a('<i class="icon md-delete" aria-hidden="true"></i>Borrar', 'javascript:void(0)', ['class' => 'dropdown-item btn-imagen-delete ajax-call', 'target' => '_BLANK', 'data-method' => 'POST', 'data-params' => '', 'data-url' => Url::toRoute(['/imagenes/borrar', 'id' => $model->id])]);
                ?>

            </div>
        </div>
        <div class="title"><?= $model->alt ?></div>
        <div class="time"><?= \common\utils\Utils::time_elapsed_string(date('d-m-Y h:m:s', $model->fecha_subida)) ?></div>
        <div class="media-item-actions btn-group">





            <?= Html::a('<i class="icon md-edit" aria-hidden="true"></i>', ['/imagenes/recortar', 'id' => $model->id], ['class' => 'btn btn-icon btn-pure btn-default remote-modal', 'target' => '_BLANK', 'data-target' => '#modal-recortes', 'data-toggle' => 'modal']);
            ?>
            <?= "" /* Html::a('<i class="icon md-download" aria-hidden="true"></i>', ['/imagenes/recortar', 'id' => $model->id], ['class' => 'btn btn-icon btn-pure btn-default remote-modal', 'target' => '_BLANK', 'data-target' => '#modal-recortes', 'data-toggle' => 'modal']);
             */ ?>
            <?= Html::a('<i class="icon md-delete" aria-hidden="true"></i>', 'javascript:void(0)', ['class' => 'btn btn-icon btn-pure btn-imagen-delete btn-default ajax-call', 'data-method' => 'POST', 'data-params' => '', 'data-url' => Url::toRoute(['/imagenes/borrar', 'id' => $model->id])]);
            ?>

        </div>
    </div>
</div>



<?php
$this->registerJs(
<<<JAVASCRIPT

   $('.btn-imagen-delete').bind("ajax-call-response",function(e){
        toastr["success"]("La imagen ha sido eliminada exitosamente!");
        $.pjax.reload({container: '#pjax-imagenes-container', timeout: 10000});
   })
        
JAVASCRIPT
);
