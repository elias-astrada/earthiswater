<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NoticiasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

\backend\assets\ImagenesAsset::register($this);

$this->title = 'Imágenes';

$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-show'] = false;
Yii::$app->params['page-content-show'] = false;
Yii::$app->params['page-body-class'] = 'app-media page-aside-left site-menubar-unfold';

$this->registerCss("
    
    #infscr-loading{
        text-align:center;
    }
    
    #list-imagenes ul.pagination {
    margin:0;
    }
    #list-imagenes ul.pagination li.next{
                width: 100%;
                text-align: center;
    }


    }
    
    ");
?>



<!--
<div class="page-aside">
    <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
    </div>
    <div class="page-aside-inner page-aside-scroll">
        <div data-role="container">
            <div data-role="content">
                <section class="page-aside-section">
                    <h5 class="page-aside-title">Main</h5>
                    <div class="list-group">
                        <a class="list-group-item active" href="javascript:void(0)"><i class="icon md-view-dashboard" aria-hidden="true"></i>Galería</a>
                        <a class="list-group-item" href="javascript:void(0)"><i class="icon md-image-o" aria-hidden="true"></i>Documentación</a>
                    </div>
                </section>
<!-- 
 <section class="page-aside-section">
     <h5 class="page-aside-title">Filter</h5>
     <div class="list-group">
         <a class="list-group-item" href="javascript:void(0)"><i class="icon md-image" aria-hidden="true"></i>Images</a>
         <a class="list-group-item" href="javascript:void(0)"><i class="icon md-volume-up" aria-hidden="true"></i>Audio</a>
         <a class="list-group-item" href="javascript:void(0)"><i class="icon md-videocam" aria-hidden="true"></i>Video</a>
         <a class="list-group-item" href="javascript:void(0)"><i class="icon md-file" aria-hidden="true"></i>Notes</a>
         <a class="list-group-item" href="javascript:void(0)"><i class="icon md-link" aria-hidden="true"></i>Links</a>
         <a class="list-group-item" href="javascript:void(0)"><i class="icon md-receipt" aria-hidden="true"></i>Files</a>
     </div>
 </section>

</div>
</div>
</div>
</div> -->

<!-- Media Content -->
<div class="page-main">
    <!-- Media Content Header -->
    <div class="page-header">
        <h1 class="page-title"><?= $this->title ?></h1>
        <div class="page-header-actions">
            <!--<form>
                <div class="input-search input-search-dark">
                    <i class="input-search-icon md-search" aria-hidden="true"></i>
                    <input type="text" class="form-control" name="" placeholder="Buscar...">
                </div>
            </form>-->
        </div>
    </div>
    <!-- Media Content -->
    <div id="mediaContent" class="page-content page-content-table" data-plugin="selectable">
        <!-- Actions -->
        <div class="page-content-actions">
            <div class="float-right">
                <div class="btn-group media-arrangement btn-group-flat" role="group">
                    <button class="btn btn-default active" id="arrangement-grid" type="button"><i class="icon md-view-module" aria-hidden="true"></i></button>
                    <button class="btn btn-default" id="arrangement-list" type="button"><i class="icon md-view-list" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="actions-inner">
                <!--<div class="checkbox-custom checkbox-primary checkbox-lg">
                    <input type="checkbox" id="media_all" class="selectable-all">
                    <label for="media_all"></label>
                </div>-->
            </div>
        </div>


        <!-- Media -->
        <?php Pjax::begin(['id' => 'pjax-imagenes-container', 'timeout' => 5000]); ?>

        <div class="media-list is-grid pb-50" data-plugin="animateList" data-animate="fade"
             data-child="li">
                 <?=
                 ListView::widget([
                     'dataProvider' => $dataProvider,
                     'id' => 'list-imagenes',
                     'layout' => '<div data-plugin="animateList" data-child=">li" class="items blocks blocks-100 blocks-xxl-4 blocks-xl-3 blocks-lg-3 blocks-md-2 blocks-sm-2">{items}</div>{pager}',
                     //'layout' => '{items}<div class="pager">{pager}</div>',
                     'itemOptions' => ['tag' => 'li', 'class' => ''],
                     'options' => [
                         'tag' => 'ul',
                     /* class' => 'blocks blocks-100 blocks-xxl-4 blocks-xl-3 blocks-lg-3 blocks-md-2 blocks-sm-2',
                       'data-plugin' => "animateList",
                       'data-child' => ">li", */
                     ],
                     'itemView' => '_thumbitem',
                     'pager' => [
                         'class' => InfiniteScrollPager::className(),
                         'widgetId' => 'list-imagenes',
                         'itemsCssClass' => 'items',
                         'nextPageLabel' => 'Cargar más imágenes',
                         'contentLoadedCallback' => 'function(data) { if(data.length < ' . $dataProvider->pagination->pageSize . ') $(".list-notas-actions").hide(); }',
                         'linkOptions' => [
                             'class' => 'btn btn-primary',
                         ],
                         'pluginOptions' => [
                             'loading' => [
                                 'msgText' => "<em>Cargando...</em>",
                                 'finishedMsg' => "<em>No se han encontrado mas resultados</em>",
                             ],
                             'behavior' => InfiniteScrollPager::BEHAVIOR_TWITTER,
                         ],
                     ],
                 ]);
                 ?>

        </div>
        <?php Pjax::end(); ?>
    </div>
</div>
</div>
<div class="site-action" data-plugin="actionBtn">

    <?= Html::a('<i class="front-icon md-upload animation-scale-up" aria-hidden="true"></i>
        <i class="back-icon md-close animation-scale-up" aria-hidden=ww"true"></i>', ['/imagenes/subir'], ['class' => 'btn-raised btn btn-success btn-floating remote-modal', 'target' => '_BLANK', 'data-target' => '#modal-subir', 'data-toggle' => 'modal']); ?>

    <div class="site-action-buttons">
        <button type="button" data-action="download" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
            <i class="icon md-download" aria-hidden="true"></i>
        </button>
        <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom ">
            <i class="icon md-delete" aria-hidden="true"></i>
        </button>
    </div>
    <input type="file" id="fileupload" name="upload" />
</div>


w

<div class="modal fade modal-scroll" id="modal-subir" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>


<div class="modal fade modal-scroll" id="modal-recortes" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>


