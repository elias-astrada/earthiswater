<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductosCategorias */

$this->title = 'Modificar Productos Categorias: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Productos Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar';
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="productos-categorias-modificar">


            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>


</div>