<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

   <!-- <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $toolbar = Html::a('<i class="glyphicon glyphicon-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']) .
            Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0)', ['id' => 'btn-bulk-del', 'class' => 'btn btn-danger', 'title' => 'Borrar Seleccionados']) .
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-default', 'title' => 'Actualizar']);
    ?>

    <?=
    GridView::widget([
        'export' => [
            'target' => GridView::TARGET_SELF,
            'label' => 'Exportar',
        ],
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $dataProvider,
        'toolbar' => [
                ['content' => $toolbar],
            '{export}',
            '{toggleData}',
        ],
        'pjax' => true,
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'panel' => [
            'heading' => '<i class="fa fa-table" aria-hidden="true"></i> ' . Html::encode($this->title),
            'type' => 'primary',
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'kartik\grid\CheckboxColumn'],
            // ['class' => 'kartik\grid\SerialColumn'],
            //           'id',
            'nombre',
            // 'descripcion:ntext',
            [
                'attribute' => 'fecha_creacion',
                'format' => ['date', 'php:d-m-Y H:i'],
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([
            'attribute' => 'date',
            'presetDropdown' => TRUE,
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => ['format' => 'd-m-Y'],
                'opens' => 'left'
            ],
                ]),
                'format' => 'date',
                'width' => '25%',
            ],
                [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'activo',
                    'hAlign' => 'center',
                'vAlign' => 'middle',
                'width' => '10%',
            ],
            //'activo:boolean',
            //'imagen',
            // 'keywords',
            // 'orden',
            // 'fecha_modificacion',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-action remote-modal', 'target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                    },
                ],
                'updateOptions' => [
                    'class' => 'btn btn-primary btn-action',
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-danger btn-action',
                ],
                'width' => '95px',
                'noWrap' => true,
            ],
        ],
    ]);
    ?>
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?= $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
        $.post(
            "delete-multiple", 
            {
                pk : $("#grid-pjax").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-pjax"});
            }
        );
      });
    });', \yii\web\View::POS_READY);
?>