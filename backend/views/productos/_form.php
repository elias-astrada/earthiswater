<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Productos */
/* @var $form yii\widgets\ActiveForm */


$this->registerCssFile(Yii::$app->request->baseUrl . '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(['id' => 'form-productos']); ?>





    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

        <div class="panel-body">


            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>





        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i>Imagen Principal</h3></div>

        <div class="panel-body">
            
                        <div class="form-group">
                <label>Imagen Principal</label>
                <?= backend\widgets\MultimediaWidget::widget(['id' => 'imgprincpwidget', 'multiple' => false, 'model' => $model, 'attribute' => 'imagen', 'previewUrl' => !is_null($model->imagen0) ? $model->imagen0->getUrl() : '']); ?>

            </div>

        </div>

    </div>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i>Multimedia</h3></div>

        <div class="panel-body">
            <?php $dataprovider_multimedia = $model->archivosMultimedia; ?>
            <?= backend\widgets\MultimediaWidget::widget(['id' => 'multimediawidget', 'unlink_url' => Url::toRoute(['/productos/multimedia-remove', 'id' => $model->id]), 'dataprovider_multimedia' => $dataprovider_multimedia, 'multiple' => true]); ?>


        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i>Configuración</h3></div>

        <div class="panel-body">

            <div class="row">
                <div class="col-md-4">


                    <div class="form-group">

                        <label>Palabras Clave</label>
                        <?=
                        Select2::widget([
                            'name' => 'palabras_clave',
                            'value' => $model->isNewRecord ? '' : explode(',', $model->keywords),
                            'options' => ['placeholder' => 'Ingrese palabras clave separadas por coma o espacio', 'multiple' => true, 'class' => 'form-control'],
                            'pluginOptions' => [
                                'tags' => true,
                                'tokenSeparators' => [',', ' '],
                                'maximumInputLength' => 10,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'orden')->input('number') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'activo')->widget(SwitchInput::classname(), []); ?>
                </div>
            </div>
        </div>
    </div>    



    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success px-100' : 'btn btn-primary px-100']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= backend\widgets\MultimediaWidget::renderModals('imgprincpwidget') ?> 


<?= backend\widgets\MultimediaWidget::renderModals('multimediawidget', true) ?> 



<?php
$this->registerJs(
        <<<JAVASCRIPT
    var fields_uploading = 0;  
    var files_uploadeds = 0;
        
    function fileinput_uploaded_event(e, data, previewId, index){

        var response = data.response;
        
        if(response.success){
        
            var inputvalue = $("#productos-multimedia-files").val();
            if(inputvalue == ""){
                 $("#productos-multimedia-files").val(response.uploadedid);
            }else{
                var filesarray = inputvalue.split(",");
                filesarray.push(response.uploadedid);
                $("#productos-multimedia-files").val(filesarray.join(','));
            }

        }

    }   
        
    $('.upload-ajax-field').on('filepreupload', function(event, data, previewId, index) {
        fields_uploading ++;
    });      
        
    $('.upload-ajax-field').on('fileuploaded', function(event, data, previewId, index) {
        
        fields_uploading -- ;
        files_uploadeds ++;
        if(fields_uploading <= 0 ){
            $('#form-productos').yiiActiveForm('submitForm');
        }
    });
        
    $('#form-productos').on('beforeSubmit', function (event, jqXHR, settings) {
        
        $('.upload-ajax-field').fileinput('upload');
        
        
        if(fields_uploading <= 0 ){
            
            return true;
        }
        
        
        return false;
    });
JAVASCRIPT
);
?>