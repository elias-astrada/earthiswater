<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ConsultasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultas-index">


    <?php

    $toolbar =Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class'=>'btn btn-default', 'title'=>'Actualizar']);?>

    <?= GridView::widget([
        'export' => [
            'target' => GridView::TARGET_SELF,
            'label' => 'Exportar',
        ],
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $dataProvider,
        'toolbar'=> [
                  ['content'=>  $toolbar],
                  '{export}',
                  '{toggleData}',
        ],
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'pjax' => true,
        'panel'=>[
            'heading'=>'<i class="fa fa-table" aria-hidden="true"></i> '.Html::encode($this->title),
            'type'=>'primary',
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\CheckboxColumn'],
           // ['class' => 'kartik\grid\SerialColumn'],

            //            'id',
            'remitente',
            'destinatario',
            'asunto',
            //'cuerpo:ntext',
            [
                'attribute' => 'fecha_creacion',
                'format' => ['date', 'php:d-m-Y H:i'],
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([
                    'attribute' => 'date',
                    'presetDropdown' => TRUE,
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'd-m-Y'],
                        'opens' => 'left'
                    ],
                ]),
                'format' => 'date',
                'width' => '25%',
            ],

                [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'visto',
                    'hAlign' => 'center',
                'vAlign' => 'middle',
                'width' => '10%',
                    'trueLabel' => 'Visto',
                    'falseLabel' => 'No visto'
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-action remote-modal', 'target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                    },
                ],
                'updateOptions' => [
                    'class' => 'btn btn-primary btn-action',
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-danger btn-action',
                ],
                'width' => '95px',
                'noWrap' => true,
            ],           
        ],
    ]); ?>
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



