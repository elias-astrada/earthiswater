<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Noticias */
/* @var $form yii\widgets\ActiveForm */

/* $this->registerCssFile(Yii::$app->request->baseUrl . '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
  $this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); */
$this->registerJsFile(Yii::$app->request->baseUrl . '/plugins/tinymce/tinymce.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>



<?php $form = ActiveForm::begin(['id' => 'noticias-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>


<div class="noticias-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

        <div class="panel-body">




            <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'copete')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'cuerpo', [])->textarea(['rows' => 10, 'class' => 'wysihtml5']) ?>
            <a href="javascript:void(0)" id="btn-img-selector-panel" class="panel-galeria-activate hidden"  data-toggle="slidePanel" data-url="<?= Url::toRoute(['/imagenes/input', 'class' => 'noticias-editor-insertimg']) ?>" ></a>







        </div>
    </div>



    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i>Imágen Principal</h3></div>

        <div class="panel-body">

            <?= backend\widgets\MultimediaWidget::widget([ 'id' => 'imgprincpwidget', 'multiple' => false , 'model' => $model , 'attribute' => 'thumb' , 'previewUrl' => !is_null($model->thumb0) ? $model->thumb0->getUrl() : '']); ?>
        </div>

    </div>


    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i>Multimedia</h3></div>

        <div class="panel-body">


            <?php $dataprovider_multimedia = $model->archivosMultimedia; ?>
            <?= backend\widgets\MultimediaWidget::widget(['id' => 'multimediawidget', 'unlink_url' => Url::toRoute(['/noticias/multimedia-remove','id' => $model->id])  ,'dataprovider_multimedia' => $dataprovider_multimedia , 'multiple' => true]); ?>
            

        </div>

    </div>



    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i>Configuración</h3></div>

        <div class="panel-body">

            <div class="row">



                <div class="col-md-4">


                    <div class="form-group">

                        <label>Palabras Clave</label>
                        <?=
                        Select2::widget([
                            'name' => 'palabras_clave',
                            'value' => $model->isNewRecord ? '' : explode(',', $model->keywords),
                            'options' => ['placeholder' => 'Ingrese palabras clave separadas por coma o espacio', 'multiple' => true, 'class' => 'form-control'],
                            'pluginOptions' => [
                                'tags' => true,
                                'tokenSeparators' => [',', ' '],
                                'maximumInputLength' => 10,
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                </div>

                <div class="col-md-2">
                    <?= $form->field($model, 'destacado')->widget(SwitchInput::classname(), []); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'activo')->widget(SwitchInput::classname(), []); ?>

                </div>
            </div>

        </div>
    </div>


    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success px-100' : 'btn btn-primary px-100']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= backend\widgets\MultimediaWidget::renderModals('imgprincpwidget') ?>    

<?= backend\widgets\MultimediaWidget::renderModals('multimediawidget',true) ?> 



<?php




$this->registerJs("tinymce.init({ 
    

    selector:'#noticias-cuerpo' ,
    language : 'es_AR',
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste imagetools emoticons'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | uploadImage | emoticons', 
    setup: function (editor) {
      editor.addButton('uploadImage', {
        text: 'Subir Imagen',
        icon: 'image',
        onclick: function () {
        
        $('.panel-galeria-activate').trigger('click');
        }
      });
      


    },
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | imageoptions ',

    });


   // tinymce.activeEditor.formatter.register('lightbox_format', {inline : 'a', attributes : { 'data-fancybox' : 'gallery' , href : '%value' } });
");


$this->registerJs(
        <<<JAVASCRIPT
         

        
        $('.btn-submit').click(function(){
            tinymce.triggerSave();
        });
        
        $(".noticias-editor-insertimg #modal-subir").bind("img-uploaded-event",function(e,data){

            var img_id = data.response.uploadedid; 
            var img_url = data.response.uploadedurl;
            $("#noticias-cuerpo").append('<img src="'+img_url+'" class="img-responsive" >'); 

         });

       $(document).on('click', '.noticias-editor-insertimg .img-input-select', function(e){ 
            var img_id = $(this).data("imgid"); 
            var img_url = $(this).data("imgurl");


            tinymce.execCommand("mceInsertContent", false, '<img src="'+img_url+'" class="img-responsive" >');
            $("#noticias-cuerpo").trigger("click");

       });

        

JAVASCRIPT
);
