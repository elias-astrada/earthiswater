<?php
/* @var $this yii\web\View */

use backend\assets\CropperAsset;
use yii\helpers\Url;

CropperAsset::register($this);
?>

<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    <h4 class="modal-title"><strong>Recortar</strong> </h4>
</div>
<div class="modal-body">





    <div class="recortes-preview row">
        <?php
        $i = 1;
        foreach ($recortes as $recorte => $size):
            ?>

            <div class="col-md-3 recorte-item">
                <div class="recorte-header">
                    <h3><?= $recorte ?> </h3><button type="button" class="btn btn-primary btn-cropper" data-recorte="<?= $recorte ?>" data-image="img-recorte-<?= $recorte ?>" ><span class="fa fa-crop"></span> </button>
                </div>
                <img id="img-recorte-<?= $recorte ?>" class="img-fluid recorte recorte-<?= $recorte ?>" src="<?= $model->getUrl($recorte) ?>">


            </div>
            <?= $i % 4 == 0 ? '<div class="row"></div>' : '' ?>
            <?php
            $i++;
        endforeach;
        ?>

    </div>




    <div class="recortes-cropper">
        <div class="recortes-cropper-tools">
            <!-- <h3>Toolbar:</h3> -->
            <div class="btn-group">

                <button type="button" class="btn btn-success btn-cropper-tool" data-action="recortar" title="Crop">
                    <span class="fa fa-crop"></span> Recortar
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="dragmode-move" title="Move">
                    <span class="fa fa-arrows"></span>
                </button>
            </div>

           <!-- <div class="btn-group">
                <button type="button" class="btn btn-primary btn-cropper-tool"  data-action="zoom-in" title="Zoom In">
                    <span class="fa fa-search-plus"></span>
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool"  data-action="zoom-out" title="Zoom Out">

                    <span class="fa fa-search-minus"></span>

                </button>
            </div>-->

            <div class="btn-group">
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="move-left"  title="Move Left">
                    <span class="fa fa-arrow-left"></span>
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="move-right"  title="Move Right">
                    <span class="fa fa-arrow-right"></span>
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="move-up"  title="Move Up">
                    <span class="fa fa-arrow-up"></span>
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="move-down"  title="Move Down">
                    <span class="fa fa-arrow-down"></span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="rotate-noclock"  title="Rotate Left">
                    <span class="fa fa-rotate-left"></span>
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="rotate-clock"  title="Rotate Right">
                    <span class="fa fa-rotate-right"></span>
                </button>
            </div>

            <!--<div class="btn-group">
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="flip-hor"  title="Flip Horizontal">
                    <span class="fa fa-arrows-h"></span>
                </button>
                <button type="button" class="btn btn-primary btn-cropper-tool" data-action="flip-ver"  title="Flip Vertical">
                    <span class="fa fa-arrows-v"></span>
                </button>
            </div>-->

        </div>
        <img id="img-original" class="img-responsive" src="<?= $model->getUrl() ?>">
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
</div>


<?php
$cropUrl = Url::toRoute(['/imagenes/recortar', 'id' => $id]);

$r_o = [];
foreach ($recortes as $recorte => $size) {

    $r_a = explode('x', $size);

    if (count($r_a) < 2)
        continue;
    $width = $r_a[0];
    $height = $r_a[1];
    
    $r_o[$recorte]['size'] = $size;
    $r_o[$recorte]['width'] = $width;
    $r_o[$recorte]['height'] = $height;
    if($height > 0)
        $r_o[$recorte]['aspectratio'] = $width / $height;
}


$recortes_options = json_encode($r_o);

$this->registerJs(<<<JAVASCRIPT
            
    var cropper = null;
    var recorte_selected = null;    
    var recortes_options = $recortes_options;
 
    initCropper('img-original',16/9,1280,720);
           

        
    $('.btn-cropper-tool').click(function(e){
        e.preventDefault();
        console.log("sapee");
        
        
        var action = $(this).data("action");
        
        switch(action){
            case "recortar": 
                actionRecortar();
            break;
            case "dragmode-move": 
                cropper.setDragMode("move");
            break;
            case "dragmode-crop": 
                cropper.setDragMode("crop");
            break;
            case "zoom-in": 
                cropper.zoom(0.1);
            break;
            case "zoom-out": 
                cropper.zoom(-0.1);
            break;
            case "move-up": 
                cropper.move(0,-10);
            break;
            case "move-down": 
                cropper.move(0,10);
            break;
            case "move-left": 
                cropper.move(-10,0);
            break;
            case "move-right": 
                cropper.move(10,0);
            break;
            case "rotate-noclock": 
                cropper.rotate(-45);
            break;
            case "rotate-clock": 
                cropper.rotate(45);
            break;
            case "flip-hor": 
                cropper.scaleX(-1);
            break;
            case "flip-ver": 
                cropper.scaleY(-1);
            break;
        }
    });    

        
        
    $('.btn-cropper').click(function(){
        
        console.log($(this).data("recorte"));
        recorte_selected = $(this).data("recorte");
        var r_o = recortes_options[recorte_selected];
        
        console.log(r_o);
        
        clearRecorteSelected();
        $(this).closest( ".recorte-item" ).addClass( "selected" );
        console.log( $(this));
        
        
        cropper.destroy();
        initCropper('img-original',r_o.aspectratio,r_o.width,r_o.height);
        
    });    

        
    function clearRecorteSelected(){
        $(".recorte-item" ).removeClass("selected");
    }
    function actionRecortar(){
        
        cropdata = cropper.getData();
        
        
        if(recorte_selected == null || recorte_selected == ""){
            return;
        }
        console.log("recortar");
        
        
        $.ajax({
          url: "$cropUrl",
          method: "POST",
          dataType :"json",
          data: {
                recorte: recorte_selected,
                cropdata: cropdata
            },
          context: document.body
        })
        .done(function(result) {
            reloadRecortes();
        });
    }
        
    function reloadRecortes(){
      
      $(".recorte").each(function( index ) {
        var urlimg = $( this ).attr("src");
        $( this ).attr("src", urlimg + "?" + new Date().getTime()); 
      });
        
    }
        
    function initCropper(imageid,aspectRatio,minWidth,minHeight){
        var image = document.getElementById(imageid);
        cropper = new Cropper(image, {
             aspectRatio: aspectRatio,
             viewMode: 1,
             checkCrossOrigin: false,
             cropBoxResizable : false,
             minCropBoxWidth: minWidth,
             minCropBoxHeight:minHeight,
            zoomable:false,
             
           crop: function(e) {
           /*  console.log(e.detail.x);
             console.log(e.detail.y);
             console.log(e.detail.width);
             console.log(e.detail.height);
             console.log(e.detail.rotate);
             console.log(e.detail.scaleX);
             console.log(e.detail.scaleY);*/
           }
     }); 
        
        

        
    }

    function destroyCropper(){
        cropper.destroy();
    } 
   
   
JAVASCRIPT
);




