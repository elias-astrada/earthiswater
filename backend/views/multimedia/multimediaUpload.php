<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Agregar Imagen</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-12">



            <?php ActiveForm::begin(['id' => 'multimedia-form-upload-' . $wid, 'action' => Url::toRoute(['/multimedia/upload-imagen']), 'options' => ['enctype' => 'multipart/form-data']]) ?>

            <input type="file" id="input-file-max-fs" name="multimedia-image-upload" data-plugin="dropify" data-max-file-size="2M"/>

            <div class="loading-cont"></div>
            <button type="submit" class="btn btn-success btn-submit-multimedia-upload float-right mt-3 waves-effect waves-classic">
                <i class="fa fa-cloud-upload" aria-hidden="true"></i> Subir

            </button>
            <?php ActiveForm::end() ?>
        </div>
    </div>

</div>





<?php
$this->registerJs(
        <<<JAVASCRIPT
      
    $('#multimedia-form-upload-$wid').on('beforeSubmit', function (event, jqXHR, settings) {
        var form = $(this);
        
        var loadingcont = $(this).find('.loading-cont');
        
        var dropify = $(this).find('#input-file-max-fs');
        
        
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: new FormData( this ),
        
            processData: false,
            contentType: false,
        
            beforeSend: function() {
                loadingcont.fadeIn(400).html(' <div class="loader vertical-align-middle loader-round-circle"></div>').fadeIn("slow");
                console.log("uploadbeforesend");
            },
        
            success: function(data) {

        
        
                var returnedData = JSON.parse(data);
        
                loadingcont.html('');

                $('#modal-multimedia-add-imagen-$wid').modal('hide');
                console.log(returnedData);
                if(returnedData.success == true){
                    select_multimedia_$wid(returnedData.uploadedurl , returnedData.uploadedid , 'img');
                }else{
                    swal("Ups!", returnedData.error_report, "error");
                }
        
                form[0].reset();
                var drEvent = dropify.dropify();
                drEvent = drEvent.data('dropify');
                drEvent.resetPreview();
                drEvent.clearElement();
        
            },

        })
        return false;
    });
        
   
        
        
JAVASCRIPT
);
?>