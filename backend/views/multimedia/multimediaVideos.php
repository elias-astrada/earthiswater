<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * Description of multimediaVideos
 *
 * @author PC03-MAXI
 */
?>


<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Agregar Video</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-12">

            <?php ActiveForm::begin(['id' => 'multimedia-form-video-' . $wid, 'action' => Url::toRoute(['/multimedia/upload-video'])]) ?>

            <div class="form-group form-material" data-plugin="formMaterial">
                <input type="text" class="form-control empty" name="multimedia-youtube-post" id ="multimedia-youtube-link">
                <label class="">Link</label>
            </div>


            <div class="loading-cont"></div>
            <button type="submit" class="btn btn-success btn-submit-multimedia-video float-right mt-3 ladda-button waves-effect waves-classic" data-style="expand-left" data-plugin="ladda" data-type="progress">
                <span class="ladda-label"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Cargar</span>
                <span class="ladda-spinner"></span><div class="ladda-progress" style="width: 105px;"></div>
            </button>

            <?php ActiveForm::end() ?>


        </div>
    </div>
</div>








<?php
$this->registerJs(
        <<<JAVASCRIPT
    $('#multimedia-form-video-$wid').on('beforeSubmit', function (event, jqXHR, settings) {
        var form = $(this);
        var loadingcont = $(this).find('.loading-cont');
        
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
        
            beforeSend: function() {
                loadingcont.fadeIn(400).html(' <div class="loader vertical-align-middle loader-round-circle"></div>').fadeIn("slow");
            },
            success: function(data) {
                var returnedData = JSON.parse(data);
        
                loadingcont.html('');
                console.log(data);
        
                $('#modal-multimedia-add-video-$wid').modal('hide');
                if(returnedData.success == true){
                    select_multimedia_$wid(returnedData.thumb_url , returnedData.uploadedid , 'vid');
                }else{
                    swal("Ups!", returnedData.error_report, "error");
                }
        
                form[0].reset();
        
            }

        })
        return false;
    });
   
JAVASCRIPT
);
?>