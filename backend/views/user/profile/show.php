<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */
$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
//$this->registerCssFile(Url::toRoute('template/assets/examples/css/pages/profile.css'));
?>
<div class="row">
    <?= $this->render('/profile_sidebar', ['profile' => $profile]) ?>

    <div class="col-lg-9">

        <!-- END PROFILE CONTENT -->        <!-- Panel -->
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i><?= Html::encode($this->title) ?></h3></div>

            <div class="panel-body" data-plugin="tabs">
                <!-- END STAT -->
                <div>
                    <h4 class="profile-desc-title"><?= $profile->name ?></h4>

                    <?php if (!empty($profile->bio)): ?>
                        <p data-info-type="bio" class="mb-10 text-nowrap">
                            <i class="fa fa-info-circle mr-10" aria-hidden="true"></i>
                            <span class="text-break"><?= $profile->bio ?></span>
                        </p>
                    <?php endif; ?>

                    <?php if (!empty($profile->public_email)): ?>

                        <p data-info-type="bio" class="mb-10 text-nowrap">
                            <i class="fa fa-envelope mr-10"></i>
                            <span class="text-break"><?= Html::a(Html::encode($profile->public_email), 'mailto:' . Html::encode($profile->public_email)) ?></span>
                        </p>

                    <?php endif; ?>

                    <?php if (!empty($profile->location)): ?>

                        <p data-info-type="bio" class="mb-10 text-nowrap">
                            <i class="fa fa-map-marker mr-10"></i>
                            <span class="text-break"><?= $profile->location ?></span>
                        </p>

                    <?php endif; ?>

                    <?php if (!empty($profile->timezone)): ?>

                        <p data-info-type="bio" class="mb-10 text-nowrap">
                            <i class="fa fa-globe mr-10"></i>
                            <span class="text-break"><?= $profile->timezone ?></span>
                        </p>
                    <?php endif; ?>


                    <?php if (!empty($profile->website)): ?>


                        <p data-info-type="bio" class="mb-10 text-nowrap">
                            <i class="fa fa-link mr-10"></i>
                            <span class="text-break"><?= $profile->website ?></span>
                        </p>

                    <?php endif; ?>



                    <p data-info-type="bio" class="mb-10 text-nowrap">
                        <i class="fa fa-key mr-10"></i>
                        <span class="text-break"> <?= $profile->user->getRolesString(); ?></span>
                    </p>


                    <p data-info-type="lastlogin" class="mb-10 text-nowrap">
                        <i class="fa fa-clock-o mr-10" aria-hidden="true"></i>
                        <span class="text-break">Último Ingreso: <?= date('d-m-Y h:m:s', $profile->user->last_login_at) ?> 
                        </span>
                    </p>

                    <p data-info-type="registerat" class="mb-10 text-nowrap">
                        <i class="fa fa-clock-o mr-10" aria-hidden="true"></i>
                        <span class="text-break">Registro: <?= date('d-m-Y', $profile->user->created_at) ?> 
                        </span>
                    </p>

                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>

</div>