<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\Url;


$user_logged = Yii::$app->user;
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;
?>
<div class="col-lg-3">
    <!-- Page Widget -->
    <div class="card card-shadow text-center">
        <div class="card-block">
            <a class="avatar avatar-lg" href="javascript:void(0)">
                <?=
                Html::img($profile->getAvatarUrl(230), [
                    'class' => 'img-responsive',
                    'alt' => $profile->user->username,
                ])
                ?>
            </a>
            <h4 class="profile-user"><?= $profile->name ?></h4>
            <p class="profile-job"><?= $profile->user->username ?></p>
            <p><?= $profile->bio ?></p>
            <!--<div class="profile-social">
                <a class="icon bd-twitter" href="javascript:void(0)"></a>
                <a class="icon bd-facebook" href="javascript:void(0)"></a>
                <a class="icon bd-dribbble" href="javascript:void(0)"></a>
                <a class="icon bd-github" href="javascript:void(0)"></a>
            </div>-->
            <p><a href="<?= Url::toRoute(['/user-messages/create', 'to' => $profile->user_id]) ?>" class="btn btn-primary">Enviar Mensaje</a></p>





                <?=
                \common\components\MenuRemark::widget([
                    'options' => [
                        'class' => 'nav nav-pills flex-column',
                    ],
                    'linkTemplate' => '<a href="{url}" class="nav-link {active}">{label}</a>',
                    'itemOptions' => ['class' => 'nav-item'],
                    'items' => [
                            ['label' => '<i class="icon-home"></i>' . Yii::t('user', 'Vista General'), 'url' => ['/user/profile/show', 'id' => $profile->user_id], 'encode' => false,],
                            [
                            'label' => '<i class="icon-settings"></i>' . Yii::t('user', 'Opciones de Cuenta'),
                            'url' => ['/user/settings/account'],
                            'encode' => false,
                            'visible' => $user_logged->id === $profile->user_id ? true : false,],
                            [
                            'label' => '<i class="icon-settings"></i>' . Yii::t('user', 'Opciones de Perfil'),
                            'url' => ['/user/settings/profile'],
                            'encode' => false,
                            'visible' => $user_logged->id === $profile->user_id ? true : false,
                        ],
                            [
                            'label' => Yii::t('user', 'Redes'),
                            'url' => ['/user/settings/networks'],
                            'visible' => $networksVisible
                        ],
                    ],
                ])
                ?>
          
        </div>
        <!--<div class="card-footer">
            <div class="row no-space">
                <div class="col-4">
                    <strong class="profile-stat-count">260</strong>
                    <span>Follower</span>
                </div>
                <div class="col-4">
                    <strong class="profile-stat-count">180</strong>
                    <span>Following</span>
                </div>
                <div class="col-4">
                    <strong class="profile-stat-count">2000</strong>
                    <span>Tweets</span>
                </div>
            </div>
        </div>-->
    </div>
    <!-- End Page Widget -->
</div>
