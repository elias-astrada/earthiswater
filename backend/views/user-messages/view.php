<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\UserMessages */

$this->title = "Mensaje de " . $model->userFrom->profile->name;
$this->params['breadcrumbs'][] = ['label' => 'Mensajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;


$userfrom_profile = $model->userFrom->profile;
?>
<div class="user-messages-view">
    <div class="panel panel-primary">

        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">

            <div class="comment media">
                <div class="pr-20">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                        <img src="<?= $userfrom_profile->getAvatarUrl(); ?>" alt="...">
                    </a>
                </div>
                <div class="comment-body media-body">
                    <a class="comment-author" href="javascript:void(0)"><?= $userfrom_profile->name ?></a>
                    <div class="comment-meta">
                        <span class="date"><?= \common\utils\Utils::time_elapsed_string_timestamp($model->fecha); ?></span>
                    </div>
                    <div class="comment-content">
                        <p><?= $model->message ?></p>
                    </div>
                    <div class="comment-actions">
                        <a class="active" href="<?= Url::toRoute(['/user-messages/create', 'to' => $model->user_from]); ?>" role="button">Reponder</a>
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>

