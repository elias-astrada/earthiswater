<?php

use yii\helpers\Url;

if (!Yii::$app->params['showNotificaciones'])
    return '';


$notificaciones = common\models\UserNotifications::getNotificaciones(Yii::$app->user->id);
$notificaciones_count = $notificaciones->count();
$notificaciones_array = $notificaciones->all();

$not_ids = array();
foreach ($notificaciones_array as $not) {
    $not_ids[] = \yii\helpers\Json::encode($not->id);
}

$this->registerJs('
    var notificaciones_opened = false;
    $("#notificaciones_dropdown").click(function (){
        if(!notificaciones_opened){
           var notificaciones = [' . implode(",", $not_ids) . '];
            $.ajax({
              type: "POST",
              dataType: "json",
              url: "' . Url::toRoute(['/notificaciones/visto']) . '",
              data: { "notificaciones" : notificaciones }
            }).done(function() {
              $( ".notificaciones_count" ).html( "0" );
            });
        }
        notificaciones_opened = true;
    });

');
?>


<li class="nav-item dropdown">
    <a id="notificaciones_dropdown" class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Notificaciones"
       aria-expanded="false" data-animation="scale-up" role="button">
        <i class="icon md-notifications" aria-hidden="true"></i>
        <span class="badge badge-pill badge-danger up"><?= $notificaciones_count ?></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
        <div class="dropdown-menu-header">
            <h5>NOTIFICACIONES</h5>
            <span class="badge badge-round badge-danger"><?= $notificaciones_count ?> Nuevas</span>
        </div>
        <div class="list-group">
            <div data-role="container">
                <div data-role="content">


                    <?php foreach ($notificaciones_array as $notificacion): ?>
                        
                        <a class="list-group-item dropdown-item" href="<?= empty($notificacion->url) ? 'javascript:void(0);' :$notificacion->url ?>" role="menuitem">
                            <div class="media">
                                <div class="pr-10">
                                    <i class="icon md-receipt bg-red-600 white icon-circle <?= $notificacion->type ?>" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading"><?= $notificacion->title ?> </h6>
                                    <time class="media-meta" datetime="<?= date('d-m-Y h:m:s', $notificacion->fecha); ?>"><?= \common\utils\Utils::time_elapsed_string_timestamp($notificacion->fecha) ; ?></time>
                                </div>
                            </div>
                        </a>
                        
                        
                    <?php endforeach; ?>
                    
                </div>
            </div>
        </div>
        <div class="dropdown-menu-footer">
            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                <i class="icon md-settings" aria-hidden="true"></i>
            </a>
            <a class="dropdown-item" href="<?= Url::toRoute(['/notificaciones/index']) ?>" role="menuitem">
                Ver todas las notificaciones
            </a>
        </div>
    </div>
</li>