<?php

use common\components\ManifestWidgetNav;
use mdm\admin\components\MenuHelper;
use yii\widgets\Menu;
use yii\helpers\Url;
?>




<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>

                <?=
                Menu::widget([
                    'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id, null, function($menu) {
                        $data = eval($menu['data']);
                        $icono = isset($data['icon']) ? '<span class="site-menu-icon fa ' . $data['icon'] . '" aria-hidden="true" ></span>' : '';
                        return [
                            'label' => $menu['name'],
                            'url' => [$menu['route']],
                            'icon' => isset($data['icon']) ? $data['icon'] : '',
                            'items' => $menu['children'],
                            'options' => ['class' => 'site-menu-item ' . (count($menu['children']) > 0 ? 'has-sub' : '')],
                            'linkOptions' => ['class' => 'animsition-link'],
                            'template' => count($menu['children']) > 0 ? '<a href="javascript:void(0)">' . $icono . '<span class="site-menu-title">{label}</span><span class="site-menu-arrow"></span></a>' : '<a href="{url}">' . $icono . '<span class="site-menu-title">{label} </span></a>',
                        ];
                    }),
                    'options' => [
                        'class' => 'site-menu',
                        'data-plugin' => 'menu',
                    ],
                    'submenuTemplate' => "\n<ul class='site-menu-sub' >\n{items}\n</ul>\n",
                ]);
                ?>


                <!--<div class="site-menubar-section">
                    <h5>
                        Milestone
                        <span class="float-right">30%</span>
                    </h5>
                    <div class="progress progress-xs">
                        <div class="progress-bar active" style="width: 30%;" role="progressbar"></div>
                    </div>
                    <h5>
                        Release
                        <span class="float-right">60%</span>
                    </h5>
                    <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-warning" style="width: 60%;" role="progressbar"></div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <div class="site-menubar-footer">
        <a href="<?= Url::toRoute(['/user/settings']); ?>" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Opciones">
            <span class="icon md-settings" aria-hidden="true"></span>
        </a>
        <a href="<?= Url::toRoute(['/user/profile']); ?>" data-placement="top" data-toggle="tooltip" data-original-title="Perfil">
            <span class="icon md-account" aria-hidden="true"></span>
        </a>
        <a href="<?= Url::toRoute(['/site/logout']); ?>" data-method="post" role="menuitem" data-placement="top" data-toggle="tooltip" data-original-title="Salir">
            <span class="icon md-power" aria-hidden="true"></span>
        </a>
    </div>
</div>


