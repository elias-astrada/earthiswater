<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use mdm\admin\components\Helper;

Helper::invalidate(); /* Limpiar Cache Permisos */

\backend\assets\TemplateAsset::register($this);
\backend\assets\ToastrAsset::register($this);



?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- END THEME LAYOUT STYLES -->
        <!--<link rel="shortcut icon" href="favicon.ico" /> </head>-->
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <script src="<?= Url::toRoute('/global/vendor/breakpoints/breakpoints.min.js', true) ?>"></script>
        <script>
            Breakpoints();
        </script>

    </head>
    <body class="animsition <?= Yii::$app->params['page-body-class'] ?>">
        <?php $this->beginBody() ?>

        <?= $this->render('remark/nav') ?>
        <?= $this->render('remark/menubar') ?>
        <?= $this->render('remark/gridmenu') ?>

        <div class="page">
            <?php if (Yii::$app->params['page-header-show'] ): ?>
            <div class="page-header">

                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => ['class' => 'breadcrumb'],
                    'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
                    'activeItemTemplate' => '<li class="breadcrumb-item active"><span>{link}</span></li>'
                ])
                ?>
                
                <?= \common\widgets\AlertSwal::widget(); ?>
 
                <?php if (Yii::$app->params['page-header-showtitle'] ): ?>
                <h1 class="page-title"><?= !empty($this->params['title-page']) ? $this->params['title-page'] : Html::encode($this->title) ?></h1>
                <?php endif; ?>

                <?php if (isset($this->params['page-header-actions'])): ?>
                
                
                    <div class="page-header-actions">
                        
                        <?= $this->params['page-header-actions']; ?>

                    </div>

                <?php endif; ?>

            </div>
            <?php endif; ?>
            
            <?php if (Yii::$app->params['page-content-show'] ): ?><div class="page-content <?= Yii::$app->params['page-content-class'] ?> "><?php endif; ?>
                <?= $content ?>
            <?php if (Yii::$app->params['page-content-show'] ): ?></div><?php endif; ?>
        </div>

        <footer class="site-footer">
            <div class="site-footer-legal">© <?= date('Y',time()) ?> <a href="http://manifesto.com.ar"><?= Yii::$app->params['webname'] ?></a></div>
            <div class="site-footer-right">
                by <a href="http://manifesto.com.ar">Manifesto Backend</a>
            </div>
        </footer>

        <?php $this->endBody() ?>


    </body>
</html>
<?php $this->endPage() ?>