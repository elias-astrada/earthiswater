<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use mdm\admin\components\MenuHelper;
use common\components\ManifestWidgetNav;

use mdm\admin\components\Helper;
Helper::invalidate();/* Limpiar Cache Permisos */

AppAsset::register($this);



$profile = Yii::$app->user->identity->profile;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- END THEME LAYOUT STYLES -->
    <!--<link rel="shortcut icon" href="favicon.ico" /> </head>-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <div class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index">
                             <img src="<?= Yii::getAlias('@web') ?>/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->


                            <?php if(Yii::$app->params['showNotificaciones'])
                               require_once(Yii::$app->basePath . '/components/layout/Notificaciones.php');
                            ?>
                            <?php if(Yii::$app->params['showMessages'])
                                require_once(Yii::$app->basePath . '/components/layout/Messages.php');
                            ?>                            
                            <?php if(Yii::$app->params['showTasks'])
                                require_once(Yii::$app->basePath . '/components/layout/Tasks.php');
                            ?>       

                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">

                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <?php if(Yii::$app->params['showUserImage']): ?>
                                        <?= Html::img($profile->getAvatarUrl(), [
                                            'class' => 'img-circle img-responsive',
                                            'alt' => $profile->user->username,
                                        ]) ?>
                                    <?php endif; ?>
                                    <span class="username username-hide-on-mobile"> <?= Yii::$app->user->identity->username ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?= Url::toRoute(['/user/profile']); ?>">
                                            <i class="icon-user"></i> Mi Perfil </a>
                                    </li>
                                    <li>
                                        <a href="<?= Url::toRoute(['/user/settings']); ?>">
                                            <i class="icon-settings"></i> Configuración </a>
                                    </li>                                    <!--<li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>-->
                                    <li>
                                        <a href="<?= Url::toRoute(['/site/logout']); ?>" data-method="post">
                                            <i class="icon-key"></i> Salir </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->

                            <?php if(Yii::$app->params['showSidebar']):?>
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->

                            <?php endif;?>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->

                            <!-- Menu: common/components -->
                <?= ManifestWidgetNav::widget([
                    'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id, null, function($menu){
                        $data = eval($menu['data']);
                        return [
                            'label' => $menu['name'], 
                            'url' => [$menu['route']],
                            'icon' => isset($data['icon']) ? $data['icon'] : '',
                            'items' => $menu['children'],
                            'options'=> ['class'=>'nav-item'],
                            'linkOptions' => ['class' => 'nav-link']
                        ];
                    }),
                    'options' => [
                        'class' => 'page-sidebar-menu',
                        'data-auto-scroll' => 'true',
                        'data-slide-speed' => '200'
                    ],
                    'containerOptions' => [
                        'class' => ''
                    ],
                    'preset' => true,
                    'iconPrefix' => 'fa ',
                    'indMenuClose' => '',
                    'indMenuOpen' => '',
                    'firstItemCssClass' => 'start',
                    'lastItemCssClass' => 'last',
                    'indItem' => ''
                ]) ?>

                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                             <?= Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'options' => ['class' => 'page-breadcrumb'],
                                'itemTemplate' => '<li>{link}<i class="fa fa-circle"></i></li>',
                                'activeItemTemplate' => '<li><span>{link}</span></li>'
                            ]) ?>
                            <!--<ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>-->
                            <?php if(Yii::$app->params['showSidebar']): ?>
                                <div class="page-toolbar">
                                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                        <i class="icon-calendar"></i>&nbsp;
                                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                </div>

                            <?php endif;?>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> 
                            <?= !empty($this->params['title-page']) ? $this->params['title-page'] : Html::encode($this->title) ?>
                            <!--Dashboard
                            <small>dashboard & statistics</small>-->
                        </h1>

                        <?= \common\widgets\AlertSwal::widget() ?>

                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN CONTENT  -->

                        <?= $content ?>

                        <!-- END CONTENT  -->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                
                <?php if(Yii::$app->params['showSidebar'])
                    require_once(Yii::$app->basePath . '/components/layout/SideBar.php');
                ?>

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> <?= date("Y"); ?> &copy;  Manifesto Backend
                    <a target="_blank" href="http://www.manifesto.com.ar">Manifesto</a> &nbsp;|&nbsp;
                    <?= Yii::$app->params['webname'] ?>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>

    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>