<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

   
    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="lead"><?= nl2br(Html::encode($message)) ?></p>
            <p>  
                Hubo un problema al procesar su solicitud , por favor intente nuevamente más tarde o consulte con el administrador.
            </p>

    </div>
    

</div>
