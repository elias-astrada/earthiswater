
<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'CMS Manifesto V2';
?>



<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xl-4 col-md-6">
        <!-- Widget Linearea Two -->
        <div class="card bg-dark text-white card-shadow" id="widgetLineareaTwo" style="color: white">
            <div class="card-block p-20 pt-10">
                <div class="clearfix">
                    <div class=" float-left py-10">
                        <i class="fa fa-home"></i> Servidor IP
                    </div>
                    <br>
                    <h3><span class="float-left text-white"><?php echo $_SERVER['HTTP_HOST'];

 ?></span></h3>
                </div>
                
            </div>
        </div>
        <!-- End Widget Linearea Two -->
    </div>
    <div class="col-xl-4 col-md-6">
        <!-- Widget Linearea Two -->
        <div class="card bg-dark text-white card-shadow" id="widgetLineareaTwo" style="color: white">
            <div class="card-block p-20 pt-10">
                <div class="clearfix">
                    <div class=" float-left py-10">
                        <i class="fa fa-info"></i> Datos generales del servidor
                    </div>
                    <br>
                    <h3><span class="float-left text-white"><?php echo $_SERVER['SERVER_SOFTWARE'];

 ?></span></h3>
                </div>
                
            </div>
        </div>
        <!-- End Widget Linearea Two -->
    </div>
    
    <div class="col-xl-4 col-md-6">
        <!-- Widget Linearea Two -->
        <div class="card bg-dark text-white card-shadow" id="widgetLineareaTwo" style="color: white">
            <div class="card-block p-20 pt-10">
                <div class="clearfix">
                    <div class=" float-left py-10">
                        <i class="fa fa-info"></i> Versión PHP
                    </div>
                    <br>
                    <h3><span class="float-left text-white"><?php echo phpversion();

 ?></span></h3>
                </div>
                
            </div>
        </div>
        <!-- End Widget Linearea Two -->
    </div>
  </div>
  <div class="jumbotron text-center">
    <div class="m-3 pt-100 pb-100">
      <h1>Bienvenidos a Manifesto CMS V2</h1>
      <a href="https://bitbucket.org/devmanifesto/proyecto-template-v2/src" target="a_blank" class="btn btn-primary"><i class="fa fa-git"></i> Ver repositorio</a>
      <br>
    </div>
  </div>

