<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title"><strong>Detalles:</strong> </h4>
</div>
<div class="modal-body">
    <div class="scroller" data-always-visible="1" data-rail-visible="1">
        <div class="row">
            <div class="col-md-12">
            
                <div class="menu-view">

                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'menuParent.name:text:Parent',
                            'name',
                            'route',
                            'order',
                        ],
                    ])
                    ?>

                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
</div>

