<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('rbac-admin', 'Assignments');
$this->params['breadcrumbs'][] = $this->title;

$toolbar = Html::a('<i class="glyphicon glyphicon-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']) .
        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-default', 'title' => 'Actualizar']);

$columns = [
        ['class' => 'yii\grid\SerialColumn'],
    $usernameField,
];
if (!empty($extraColumns)) {
    $columns = array_merge($columns, $extraColumns);
}
$columns[] = [
    'class' => 'kartik\grid\ActionColumn',
    'template' => '{view}',
    'buttons' => [
        'view' => function ($url, $model, $key) {
            return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-action ']);
        },
    ],
    'updateOptions' => [
        'class' => 'btn btn-primary btn-action',
    ],
    'deleteOptions' => [
        'class' => 'btn btn-danger btn-action',
    ],
    'width' => '95px',
    'noWrap' => true,
];
?>
<div class="assignment-index">

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'toolbar' => [
                ['content' => $toolbar],
            '{export}',
            '{toggleData}',
        ],
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'pjax' => true,
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'panel' => [
            'heading' => '<i class="fa fa-table" aria-hidden="true"></i> ' . Html::encode($this->title),
            'type' => 'primary',
            'footer' => false,
        ],
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]);
    ?>
    <?php Pjax::end(); ?>

</div>
<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>