<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\BannersZonas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-zonas-form">

    <?php $form = ActiveForm::begin(); ?>
    
        <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

        <div class="panel-body">
            
            

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'activo')->widget(SwitchInput::classname(), []); ?>

            
        </div>
        
        </div>
    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success px-100' : 'btn btn-primary px-100']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
