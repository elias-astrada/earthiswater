<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BannersZonas */

$this->title = 'Crear Banners Zonas';
$this->params['breadcrumbs'][] = ['label' => 'Banners Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="banners-zonas-crear">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>


</div>