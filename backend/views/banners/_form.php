<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

        <div class="panel-body">


            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'html')->textarea(['rows' => 6]) ?>


            <div class="form-group">
                <label>Imagen</label>
                <?= backend\widgets\MultimediaWidget::widget(['id' => 'imgprincpwidget', 'multiple' => false, 'model' => $model, 'attribute' => 'imagen', 'previewUrl' => !is_null($model->imagen0) ? $model->imagen0->getUrl() : '']); ?>
            </div>
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'activo')->widget(SwitchInput::classname(), []); ?>


        </div>
    </div>
    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success px-100' : 'btn btn-primary px-100']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?= backend\widgets\MultimediaWidget::renderModals('imgprincpwidget') ?>    

