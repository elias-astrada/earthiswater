<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */

$this->title = $model->id;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title"><strong>Detalles:</strong> </h4>
</div>
<div class="modal-body">
    <div class="scroller" data-always-visible="1" data-rail-visible="1">
        <div class="row">
            <div class="col-md-12">


                <div class="banners-view">


                    <?=
                    DetailView::widget([
                        'condensed' => true,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'model' => $model,
                        'responsive' => true,
                        'vAlign' => 'center',
                        'attributes' => [
                            'id',
                            'nombre',
                            'html:ntext',
                            'imagen',
                            'link',
                            'activo:boolean',
                        ],
                    ])
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
</div>


