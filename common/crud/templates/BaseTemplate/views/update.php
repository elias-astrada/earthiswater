<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Modificar {modelClass}: ', ['modelClass' => Inflector::camel2words(StringHelper::basename($generator->modelClass))]) ?> . $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model-><?= $generator->getNameAttribute() ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = <?= $generator->generateString('Modificar') ?>;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-modificar">
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= "<?= " ?>Html::encode($this->title) ?></h3></div>

        <div class="panel-body">
            <?= "<?= " ?>$this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>