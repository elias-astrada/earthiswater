<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

   <!-- <h1><?= "<?= " ?>Html::encode($this->title) ?></h1>-->
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

    <?php
    echo "<?php\n";
    ?>

    $toolbar =  Html::a('<i class="glyphicon glyphicon-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']). 
                Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0)' , ['id' => 'btn-bulk-del', 'class' => 'btn btn-danger', 'title'=>'Borrar Seleccionados']) . 
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class'=>'btn btn-default', 'title'=>'Actualizar']);?>

<?= $generator->enablePjax ? '<?php Pjax::begin(); ?>' : '' ?>
<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'export' => [
            'target' => GridView::TARGET_SELF,
            'label' => 'Exportar',
        ],
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $dataProvider,
        'toolbar'=> [
                  ['content'=>  $toolbar],
                  //'{export}',
                  '{toggleData}',
        ],
        'pjax' => true,
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'panel'=>[
            'heading'=>'<i class="fa fa-table" aria-hidden="true"></i> '.Html::encode($this->title),
            'type'=>'primary',
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'kartik\grid\CheckboxColumn'],
            //['class' => 'kartik\grid\SerialColumn'],

            <?php
            $count = 0;
            if (($tableSchema = $generator->getTableSchema()) === false) {
                foreach ($generator->getColumnNames() as $name) {
                    if (++$count < 6) {
                        if(strpos($name, 'fecha')!== false ){
                            echo "
                                [
                                    'attribute' => '".$name."',
                                    'format' => ['date', 'php:d-m-Y H:i'],
                                    'filterType' => GridView::FILTER_DATE_RANGE,
                                    'filterWidgetOptions' => ([
                                        'attribute' => 'date',
                                        'presetDropdown' => TRUE,
                                        'convertFormat' => true,
                                        'pluginOptions' => [
                                            'locale' => ['format' => 'd-m-Y'],
                                            'opens' => 'left'
                                        ],
                                    ]),
                                    'format' => 'date',
                                ],
                            ";
                        }else{
                            echo "            '" . $name . "',\n";  
                        }

                    } else {
                        echo "            // '" . $name . "',\n";
                    }
                }
            } else {
                foreach ($tableSchema->columns as $column) {
                    $format = $generator->generateColumnFormat($column);
                    if (++$count < 6) {
                        echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                    } else {
                        echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                    }
                }
            }
            ?>

            
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                  'view' => function ($url, $model, $key) {
                      return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-action remote-modal','target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                  },
                ],
                'updateOptions' => [
                  'class' => 'btn btn-primary btn-action',  
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-danger btn-action',
                ],
                          
                'width' => '95px',
                'noWrap' => true,
            ], 
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?= "<?= " ?>
   $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
        $.post(
            "delete-multiple", 
            {
                pk : $("#grid-pjax").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-pjax"});
            }
        );
      });
    });', \yii\web\View::POS_READY);
?>