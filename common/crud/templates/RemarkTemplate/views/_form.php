<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= "<?= " ?>Html::encode($this->title) ?></h3></div>

        <div class="panel-body">



            <?php
            foreach ($generator->getColumnNames() as $attribute) {
                if (in_array($attribute, $safeAttributes)) {
                    echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                }
            }
            ?>





        </div>

    </div>
    <div class="form-group text-center">
        <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Crear') ?> : <?= $generator->generateString('Modificar') ?>, ['class' => $model->isNewRecord ? 'btn btn-success px-100' : 'btn btn-primary px-100']) ?>
    </div>     

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
