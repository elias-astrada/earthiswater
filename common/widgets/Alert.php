<?php
namespace common\widgets;

use Yii;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class Alert extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'error'   => 'alert-danger',
        'danger'  => 'alert-danger',
        'success' => 'alert-success',
        'info'    => 'alert-info',
        'warning' => 'alert-warning'
    ];

    public $titulos = [
        'error'   => 'Ups! Algo ha ocurrido!',
        'danger'  => 'Ups! Algo ha ocurrido!',
        'success' => 'Perfecto!',
        'info'    => 'Aviso!',
        'warning' => 'Advertencia!'
    ];


    public $iconos = [
        'error'   => 'glyphicon glyphicon-remove-sign',
        'danger'  => 'glyphicon glyphicon-remove-sign',
        'success' => 'glyphicon glyphicon-ok-sign',
        'info'    => 'glyphicon glyphicon-info-sign',
        'warning' => 'glyphicon glyphicon-exclamation-sign'
    ];


    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];


    public function init()
    {
        parent::init();

        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();
        $appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array) $data;
                foreach ($data as $i => $message) {
                    /* initialize css class for each alert box */
                    $this->options['class'] = $this->alertTypes[$type] . $appendCss;

                    /* assign unique id to each alert box */
                    $this->options['id'] = $this->getId() . '-' . $type . '-' . $i;

                    echo \kartik\widgets\Alert::widget([
                        'type' => $this->alertTypes[$type],
                        'title' => $this->titulos[$type],
                        'body' => $message,
                        'icon' => $this->iconos[$type],
                        'closeButton' => $this->closeButton,
                        'showSeparator' => true,
                        'options' => $this->options,
                        'delay' => false,
                        
                    ]);
                }

                $session->removeFlash($type);
            }
        }
    }
}
