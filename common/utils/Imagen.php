<?php

namespace common\utils;

use Yii;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Description of Imagenes
 *
 * @author PC03-MAXI
 */
class Imagen extends \common\utils\Archivo {

    
    
    public function generarRecortes($recortesdata = null) {


        if (is_null($recortesdata))
            $r_p = $this->getRecortes();
        else
            $r_p = $recortesdata;


        foreach ($r_p as $r => $size) {

            switch ($r) {
                case '16/9x2':
                    $cropresult = $this->crop($size,$this->getFilenameSinExtension().'@2.'.$this->extension);
                    break;
                case '16/9x1':
                    $cropresult = $this->crop($size,$this->getFilenameSinExtension().'@1.'.$this->extension);
                    break;
                case '4/3x1':
                    $cropresult = $this->crop($size,$this->getFilenameSinExtension().'@43.'.$this->extension);
                    break;
                case 'landscape':
                    $cropresult = $this->crop($size,$this->getFilenameSinExtension().'@ls.'.$this->extension);
                    break;
                default:
                    $cropresult = $this->crop($size);
                    break;
            }


            if (!$cropresult) {
                Yii::error('Error al recortar imagen');
            }
        }
    }

    public static function getRecortes() {
        return Yii::$app->params['recortes'];
    }

    public static function getRecorteSize($recorte){
        $recortes = self::getRecortes();
        Yii::error($recorte);
        Yii::error(print_r($recortes));
        return $recortes[$recorte];
    }

    
    public function getFiletoRecorte($recorte){
        switch ($recorte) {
            case '16/9x2': return $this->getFilenameSinExtension().'@2.'.$this->extension;
            case '16/9x1': return $this->getFilenameSinExtension().'@1.'.$this->extension;
            case '4/3x1': return $this->getFilenameSinExtension().'@43.'.$this->extension;
            case 'landscape': return $this->getFilenameSinExtension().'@ls.'.$this->extension;
            default: 
                $r_a = explode('x', $this->getRecorteSize($recorte));

                if (count($r_a) < 2)
                    return false;
                $width = $recorte[0];
                $height = $recorte[1];


                return $width . 'x' . $height . '/' . $this->filename;
        }
    }
    public function cropmanual($width, $height, $x, $y, $fileto = null) {


        if (is_null($fileto)) {
            $filename = $width . 'x' . $height . '/' . $this->filename;

            if (!file_exists(Yii::getAlias('@uploads') . $this->route . $width . 'x' . $height . '/'))
                mkdir(Yii::getAlias('@uploads') . $this->route . $width . 'x' . $height . '/', 0777, true);
                        
        } else {
            $filename = $fileto;
        }

        $file = $this->getFile();
        $file_save = Yii::getAlias('@uploads') . $this->route . $filename;
        return Image::crop($file, $width, $height, [$x, $y])->save($file_save, ['quality' => 80]);
    }

    public function deleteRecortes($recortesdata = null) {
        if (is_null($recortesdata))
            $r_p = $this->getRecortes();
        else
            $r_p = $recortesdata;


        foreach ($r_p as $r => $size) {

            switch ($r) {
                case '16/9x2':

                    $file = Yii::getAlias('@uploads') . $this->route . $this->getFilenameSinExtension().'@2.'.$this->extension;
                    break;
                case '16/9x1':

                    $file = Yii::getAlias('@uploads') . $this->route . $this->getFilenameSinExtension().'@1.'.$this->extension;
                    break;
                case '4/3x1':

                    $file = Yii::getAlias('@uploads') . $this->route . $this->getFilenameSinExtension().'@43.'.$this->extension;
                    break;
                case 'landscape':

                    $file = Yii::getAlias('@uploads') . $this->route . $this->getFilenameSinExtension().'@ls.'.$this->extension;
                    break;
                default:

                    $file = Yii::getAlias('@uploads') . $this->route . $size . '/' . $this->filename;
                    break;
            }



            if (is_file($file))
                unlink($file);
        }
    }

    public function delete($deleterecortes = true) {
        //Borrar recortes
        if ($deleterecortes) {
            $this->deleteRecortes();
        }
        //borrar original
        $file = $this->getFile();
        if (is_file($file))
            unlink($file);
        if (!empty($this->db_id)) {
            $f_db = $this->getDbRecord();

            if (!is_null($f_db))
                $f_db->delete();
        }
    }

    public function crop($dimension, $fileto = null) {



        $recorte = explode('x', $dimension);

        if (count($recorte) < 2)
            return false;
        $rec_w = $recorte[0];
        $rec_h = $recorte[1];


        if (is_null($fileto))
            $filename = $dimension . $this->filename;
        else {
            $filename = $fileto;
        }


        if (!file_exists(Yii::getAlias('@uploads') . $this->route . 'temp/'))
            mkdir(Yii::getAlias('@uploads') . $this->route . 'temp/', 0777, true);

        //Copiamos el original:
        $temp = Yii::getAlias('@uploads') . $this->route . 'temp/' . $filename;

        if (!copy(Yii::getAlias('@uploads') . $this->route . $this->filename, $temp))
            return false;

        //calculamos el ratio y las dimensiones finales
        $convertedSizes = $this->calculateResizeImage($temp, $rec_w, $rec_h);
        //hacemos el recorte sobre la copia.
        if ($convertedSizes)
            $this->smart_resize_image($temp, null, $rec_w, $rec_h, false, $temp, false, false, 91);
        else
            return false;




        if (is_null($fileto)) {
            //Si la carpeta del recorte no existe la creamos
            if (!file_exists(Yii::getAlias('@uploads') . $this->route . $dimension . '/'))
                mkdir(Yii::getAlias('@uploads') . $this->route . $dimension . '/', 0777, true);
        }


        $dest = Yii::getAlias('@uploads') . $this->route . $filename;
        //Movemos la copia:
        rename($temp, $dest);
        return true;
    }

    private function smart_resize_image($file, $string = null, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false, $quality = 100
    ) {

        if ($height <= 0 && $width <= 0)
            return false;
        if ($file === null && $string === null)
            return false;

        # Setting defaults and meta
        $info = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
        $image = '';
        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;
        $cropHeight = $cropWidth = 0;

        # Calculating proportionality
        if ($proportional) {
            if ($width == 0)
                $factor = $height / $height_old;
            elseif ($height == 0)
                $factor = $width / $width_old;
            else
                $factor = min($width / $width_old, $height / $height_old);

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        }
        else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
            $widthX = $width_old / $width;
            $heightX = $height_old / $height;

            $x = min($widthX, $heightX);
            $cropWidth = ($width_old - $width * $x) / 2;
            $cropHeight = ($height_old - $height * $x) / 2;
        }

        # Loading image to memory according to type
        switch ($info[2]) {
            case IMAGETYPE_JPEG: $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);
                break;
            case IMAGETYPE_GIF: $file !== null ? $image = imagecreatefromgif($file) : $image = imagecreatefromstring($string);
                break;
            case IMAGETYPE_PNG: $file !== null ? $image = imagecreatefrompng($file) : $image = imagecreatefromstring($string);
                break;
            default: return false;
        }


        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image);
            $palletsize = imagecolorstotal($image);

            if ($transparency >= 0 && $transparency < $palletsize) {
                $transparent_color = imagecolorsforindex($image, $transparency);
                $transparency = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            } elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


        # Taking care of original, if needed
        if ($delete_original) {
            if ($use_linux_commands)
                exec('rm ' . $file);
            else
                @unlink($file);
        }

        # Preparing a method of providing result
        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = $file;
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        # Writing image according to type to the output destination and image quality
        switch ($info[2]) {
            case IMAGETYPE_GIF: imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG: imagejpeg($image_resized, $output, $quality);
                break;
            case IMAGETYPE_PNG:
                $quality = 9 - (int) ((0.9 * $quality) / 10.0);
                imagepng($image_resized, $output, $quality);
                break;
            default: return false;
        }

        return true;
    }

    private function calculateResizeImage($url, $maxWidth, $maxHeight) {
        if (!empty($url) && !empty($maxWidth) && !empty($maxHeight)) {
            $size = getimagesize($url);

            if (!empty($size)) {
                $imageWidth = $size[0];
                $imageHeight = $size[1];
                $wRatio = $imageWidth / $maxWidth;
                $hRatio = $imageHeight / $maxHeight;
                $maxRatio = max($wRatio, $hRatio);

                if ($maxRatio > 1) {
                    $outputWidth = $imageWidth / $maxRatio;
                    $outputHeight = $imageHeight / $maxRatio;
                } else {
                    $outputWidth = $imageWidth;
                    $outputHeight = $imageHeight;
                }

                $converted = array(
                    'width' => $outputWidth,
                    'height' => $outputHeight
                );

                return $converted;
            }
        }

        return false;
    }

}
