<?php

return [
    //DEFAULT PARAMS YII2
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    //MANIFESTO
    'ruta_base' => '/proyecto-template/',
    'webname' => 'ManifestoTemplate',
    'user' => [
        'enableEmptyPassword' => false,
    ],
];
