<?php

use \kartik\datecontrol\Module;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'es-ES',
    'sourceLanguage' => 'es-ES',
    
    'homeUrl' => ['site/index'], //Importante para los redireccionamientos de login y register (ver metodo goHome() )
    'components' => [
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'user' => [
            'identityClass' => 'common\models\User', // User must implement the IdentityInterface
            'enableAutoLogin' => true,
            'loginUrl' => ['user/security/login'],
        /* 'on '.\yii\web\User::EVENT_AFTER_LOGOUT => ['common\events\UserEvents', 'handleAfterLogout'],
          'on '.\yii\web\User::EVENT_AFTER_LOGIN => ['common\events\UserEvents', 'handleAfterLogin'],
          'on '.\frontend\controllers\users\RecoveryController::EVENT_AFTER_REQUEST => ['common\events\UserEvents', 'handleAfterRequest'], */
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.secure-sandbox.com',
                'username' => 'test@secure-sandbox.com',
                'password' => 'sim138',
                'port' => '25',
            ],
        ],
    ],
    'modules' => [
        'datecontrol' => [
            'class' => 'kartik\datecontrol\Module',
            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                Module::FORMAT_DATE => 'dd-MM-yyyy',
                Module::FORMAT_TIME => 'H:i',
                Module::FORMAT_DATETIME => 'dd-MM-yyyy H:i',
            ],
            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                Module::FORMAT_DATE => 'php:U', // saves as unix timestamp
                Module::FORMAT_TIME => 'php:U',
                Module::FORMAT_DATETIME => 'php:U',
            ],
            'ajaxConversion' => true,
            'autoWidget' => true,
            // default settings for each widget from kartik\widgets used when autoWidget is true
            'autoWidgetSettings' => [
                Module::FORMAT_DATE => ['type' => 2, 'pluginOptions' => ['autoclose' => true]], // example
                Module::FORMAT_DATETIME => ['type' => 2, 'pluginOptions' => ['autoclose' => true]], // setup if needed
                Module::FORMAT_TIME => ['type' => 2, 'pluginOptions' => ['autoclose' => true]], // setup if needed
            ]
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => false,
            'enableRegistration' => false,
            'enablePasswordRecovery' => true,
            'viewPath' => '@app/views/user',
            'controllerMap' => [
                'admin' => 'common\controllers\users\AdminController',
                'profile' => 'common\controllers\users\ProfileController',
                'recovery' => 'common\controllers\users\RecoveryController',
                'registration' => 'common\controllers\users\RegistrationController',
                'security' => 'common\controllers\users\SecurityController',
                'settings' => 'common\controllers\users\SettingsController',
            ],
            'modelMap' => [
                'Profile' => 'common\models\Profile',
                'User' => 'common\models\User',
                'UserSearch' => 'common\models\UserSearch',
                'LoginForm' => 'common\models\LoginForm',
                'RegistrationForm' => 'common\models\RegistrationForm',
                'ResendForm' => 'common\models\ResendForm',
                'SettingsForm' => 'common\models\SettingsForm',
            ],
            'mailer' => [
                'sender' => 'dev@manifesto.com.ar', // or ['no-reply@myhost.com' => 'Sender name']
                'welcomeSubject' => 'Gracias por registrarte',
                'confirmationSubject' => 'Confirmación de email',
                'reconfirmationSubject' => 'Cambio de email',
                'recoverySubject' => 'Recuperar contraseña',
                'viewPath' => '@common/mail',
            ],
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'generators' => [
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        
                        'BaseTemplate' => '@common/crud/templates/BaseTemplate',
                        'RemarkTemplate' => '@common/crud/templates/RemarkTemplate'
                        
                        
                        ]
                ]
            ],
        ],
    ],
];
