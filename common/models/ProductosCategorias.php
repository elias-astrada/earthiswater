<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "productos_categorias".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $orden
 * @property integer $activo
 *
 * @property ProductosXCategorias[] $productosXCategorias
 * @property Productos[] $productos
 */
class ProductosCategorias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productos_categorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'orden', 'activo'], 'required'],
            [['orden', 'activo'], 'integer'],
            [['nombre'], 'string', 'max' => 96],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'orden' => 'Orden',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductosXCategorias()
    {
        return $this->hasMany(ProductosXCategorias::className(), ['categoria_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['id' => 'producto_id'])->viaTable('productos_x_categorias', ['categoria_id' => 'id']);
    }
}
