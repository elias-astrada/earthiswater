<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "productos_multimedia".
 *
 * @property integer $producto_id
 * @property integer $archivos_id
 *
 * @property Archivos $archivos
 * @property Productos $producto
 */
class ProductosMultimedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productos_multimedia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_id', 'archivos_id'], 'required'],
            [['producto_id', 'archivos_id'], 'integer'],
            [['archivos_id'], 'exist', 'skipOnError' => true, 'targetClass' => Archivos::className(), 'targetAttribute' => ['archivos_id' => 'id']],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'producto_id' => 'Producto ID',
            'archivos_id' => 'Archivos ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArchivos()
    {
        return $this->hasOne(Archivos::className(), ['id' => 'archivos_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['id' => 'producto_id']);
    }
}
