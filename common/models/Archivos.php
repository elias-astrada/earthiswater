<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "archivos".
 *
 * @property integer $id
 * @property string $ruta
 * @property string $nombre
 * @property string $extension
 * @property string $alt
 * @property integer $fecha_subida
 * @property integer $url
 *
 * @property Profile[] $profiles
 */
class Archivos extends \yii\db\ActiveRecord {

    const IMAGES_EXTENSIONS = ['jpg', 'jpeg', 'png', 'bmp', 'svg', 'jps', 'gif'];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'archivos';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['ruta', 'nombre', 'fecha_subida', 'url'], 'required'],
                [['fecha_subida', 'url','imagen_recortes'], 'integer'],
                [['tipo'],'string'],
                [['ruta', 'nombre', 'extension', 'alt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'ruta' => 'Ruta',
            'nombre' => 'Nombre',
            'extension' => 'Extension',
            'alt' => 'Alt',
            'fecha_subida' => 'Fecha Subida',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles() {
        return $this->hasMany(Profile::className(), ['avatar' => 'id']);
    }

    public function getArchivohelper() {

        if ($this->isImagen()) {
            return \common\utils\Imagen::loaddb($this->id);
        } else {
            return \common\utils\Archivo::loaddb($this->id);
        }
    }

    public function getUrl($recorte = null) {
        if ($this->isImagen() && !is_null($recorte)) {
            $image_helper = $this->getArchivohelper();
            switch ($recorte) {
                case '16/9x2':
                    return Yii::getAlias('@uploadsUrl') . $this->ruta . $image_helper->getFilenameSinExtension() . '@2.' . $this->extension;
                case '16/9x1':
                    return Yii::getAlias('@uploadsUrl') . $this->ruta . $image_helper->getFilenameSinExtension() . '@1.' . $this->extension;
                case '4/3x1':
                    return Yii::getAlias('@uploadsUrl') . $this->ruta . $image_helper->getFilenameSinExtension() . '@43.' . $this->extension;
                case 'landscape':
                    return Yii::getAlias('@uploadsUrl') . $this->ruta . $image_helper->getFilenameSinExtension() . '@ls.' . $this->extension;
                default:
                    return Yii::getAlias('@uploadsUrl') . $this->ruta . $image_helper->getRecorteSize($recorte) . '/' . $this->nombre;
            }
        }
        return Yii::getAlias('@uploadsUrl') . $this->ruta . $this->nombre;
    }

    public function getAbsoluteUrl() {

        return Yii::$app->urlManagerFront->createAbsoluteUrl('uploads'.$this->ruta . $this->nombre);
    }

    public function afterDelete() {
        $file_helper = $this->getArchivohelper();
        if (!is_null($file_helper))
            $file_helper->delete();
    }

    public function isImagen() {
        if (in_array($this->extension, Archivos::IMAGES_EXTENSIONS)) {
            return true;
        }
        return false;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchImagenes() {
        $query = Archivos::find()->where(['in', 'extension', self::IMAGES_EXTENSIONS])->orderBy(['fecha_subida' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }

}
