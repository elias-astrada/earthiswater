-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-04-2018 a las 16:59:03
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectotemplate`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE `archivos` (
  `id` int(11) NOT NULL,
  `ruta` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `fecha_subida` int(11) NOT NULL,
  `url` tinyint(1) NOT NULL,
  `imagen_recortes` tinyint(4) NOT NULL,
  `tipo` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `archivos`
--

INSERT INTO `archivos` (`id`, `ruta`, `nombre`, `extension`, `alt`, `fecha_subida`, `url`, `imagen_recortes`, `tipo`) VALUES
(2, '/galeria/', '59dd0ff465c0c.jpg', 'jpg', '', 1507659764, 0, 0, NULL),
(12, '/galeria/', '59ef6c0e4306e.jpg', 'jpg', '', 1508862990, 0, 0, NULL),
(13, '/galeria/', '59ef6c1a23e58.jpg', 'jpg', '', 1508863002, 0, 0, NULL),
(15, '/galeria/', '59ef91cd60705.jpg', 'jpg', '', 1508872653, 0, 0, NULL),
(16, '/galeria/', '59efb73a0a0d2.jpg', 'jpg', '', 1508882234, 0, 0, NULL),
(17, '/galeria/', '59f0dcee2aeea.jpg', 'jpg', '', 1508957422, 0, 0, NULL),
(18, '/galeria/', '59f0eab756863.jpg', 'jpg', '', 1508960951, 0, 0, NULL),
(19, '/galeria/', '59f77792091ab.jpg', 'jpg', '', 1509390226, 0, 0, NULL),
(20, '/galeria/', '59f7781d9c528.jpg', 'jpg', '', 1509390365, 0, 0, NULL),
(21, '/galeria/', '59f7799d25236.jpg', 'jpg', '', 1509390749, 0, 0, NULL),
(22, '/galeria/', '59f78452cbc37.jpg', 'jpg', '', 1509393490, 0, 0, NULL),
(23, '/galeria/', '59f784646e5fd.jpg', 'jpg', '', 1509393508, 0, 0, NULL),
(24, '/galeria/', '59f78485b3f00.jpg', 'jpg', '', 1509393541, 0, 0, NULL),
(25, '/galeria/', '59f785042280e.jpg', 'jpg', '', 1509393668, 0, 0, NULL),
(26, '/galeria/', '59f7856c1b89c.jpg', 'jpg', '', 1509393772, 0, 0, NULL),
(27, '/galeria/', '59f7859447294.jpg', 'jpg', '', 1509393812, 0, 0, NULL),
(28, '/galeria/', '59f785d2deddd.jpg', 'jpg', '', 1509393874, 0, 0, NULL),
(29, '/galeria/', '59f7871fd896f.jpg', 'jpg', '', 1509394207, 0, 0, NULL),
(30, '/galeria/', '59f7889d680c5.jpg', 'jpg', '', 1509394589, 0, 0, NULL),
(31, '/galeria/', '59f788b93765c.jpg', 'jpg', '', 1509394617, 0, 0, NULL),
(32, '/galeria/', '59f789c86331a.jpg', 'jpg', '', 1509394888, 0, 0, NULL),
(33, '/galeria/', '59f78a17e94bf.jpg', 'jpg', '', 1509394968, 0, 0, NULL),
(34, '/galeria/', '59f78a7ba403d.jpg', 'jpg', '', 1509395067, 0, 0, NULL),
(35, '/galeria/', '59f78ae4b2580.jpg', 'jpg', '', 1509395172, 0, 0, NULL),
(36, '/galeria/', '59f78bee7311f.jpg', 'jpg', '', 1509395438, 0, 0, NULL),
(37, '/galeria/', '59f78c21046f9.jpg', 'jpg', '', 1509395489, 0, 0, NULL),
(38, '/galeria/', '59f78c455563a.jpg', 'jpg', '', 1509395525, 0, 0, NULL),
(39, '/galeria/', '5a05c2c70d1c2.jpg', 'jpg', '', 1510326983, 0, 0, NULL),
(40, '/noticias-multimedia/', '5a0c6af230192.jpg', 'jpg', '', 1510763250, 0, 0, NULL),
(41, '/noticias-multimedia/', '5a0c6da42dfc1.jpg', 'jpg', '', 1510763940, 0, 0, NULL),
(42, '/noticias-multimedia/', '5a0c6df730fdf.jpg', 'jpg', '', 1510764023, 0, 0, NULL),
(43, '/noticias-multimedia/', '5a0c6e86125bb.jpg', 'jpg', '', 1510764166, 0, 0, NULL),
(44, '/noticias-multimedia/', '5a0c6eeb91db2.jpg', 'jpg', '', 1510764267, 0, 0, NULL),
(45, '/noticias-multimedia/', '5a0c6eebccdb4.jpg', 'jpg', '', 1510764267, 0, 0, NULL),
(46, '/noticias-multimedia/', '5a0c6eec3ceac.jpg', 'jpg', '', 1510764268, 0, 0, NULL),
(47, '/noticias-multimedia/', '5a0c7228aad15.jpg', 'jpg', '', 1510765096, 0, 0, NULL),
(48, '/noticias-multimedia/', '5a0c7a34b35ea.jpg', 'jpg', '', 1510767156, 0, 0, NULL),
(49, '/noticias-multimedia/', '5a0c7a35097d3.jpg', 'jpg', '', 1510767157, 0, 0, NULL),
(50, '/noticias-multimedia/', '5a0c7aafb8aaa.jpg', 'jpg', '', 1510767279, 0, 0, NULL),
(51, '/noticias-multimedia/', '5a0c8057a6d3a.jpg', 'jpg', '', 1510768727, 0, 0, NULL),
(52, '/noticias-multimedia/', '5a0c805806437.jpg', 'jpg', '', 1510768728, 0, 0, NULL),
(53, '/noticias-multimedia/', '5a0c808f831bf.jpg', 'jpg', '', 1510768783, 0, 0, NULL),
(54, '/noticias-multimedia/', '5a0c808ff0dc7.jpg', 'jpg', '', 1510768784, 0, 0, NULL),
(55, '/productos-multimedia/', '5a0ee658a023c.jpg', 'jpg', '', 1510925912, 0, 0, NULL),
(56, '/productos-multimedia/', '5a0ee6594419a.jpg', 'jpg', '', 1510925913, 0, 0, NULL),
(57, '/productos-multimedia/', '5a1834ab74515.jpg', 'jpg', '', 1511535787, 0, 0, NULL),
(58, '/productos-multimedia/', '5a1834aba8cf2.jpg', 'jpg', '', 1511535787, 0, 0, NULL),
(59, '/galeria/', '5a5cfed023714.jpg', 'jpg', '', 1516043984, 0, 0, NULL),
(60, '/galeria/', '5a5e5918e8189.jpg', 'jpg', '', 1516132632, 0, 0, NULL),
(61, '/galeria/', '5a5e6fbde9a04.jpg', 'jpg', '', 1516138429, 0, 0, NULL),
(62, '/galeria/', '5a5e703b551dd.jpg', 'jpg', '', 1516138555, 0, 0, NULL),
(63, '/galeria/', '5a5e7a9998758.jpg', 'jpg', '', 1516141209, 0, 0, NULL),
(64, '/galeria/', '5a5f76641a3be.jpg', 'jpg', '', 1516205668, 0, 0, NULL),
(70, '/galeria/', '5a5f907489ccb.jpg', 'jpg', '', 1516212340, 0, 0, NULL),
(71, '/galeria/', '5a5f925573e96.jpg', 'jpg', '', 1516212821, 0, 0, NULL),
(72, '/galeria/', '5a5f9379c8934.jpg', 'jpg', '', 1516213113, 0, 0, NULL),
(73, '/galeria/', '5a5f93a82b5aa.jpg', 'jpg', '', 1516213160, 0, 0, NULL),
(74, '/galeria/', '5a5f940498dde.jpg', 'jpg', '', 1516213252, 0, 0, NULL),
(75, '/galeria/', '5a5f9463ade55.jpg', 'jpg', '', 1516213347, 0, 0, NULL),
(76, '/galeria/', '5a5f949533d7f.jpg', 'jpg', '', 1516213397, 0, 0, NULL),
(77, '/galeria/', '5a5f9517b7088.jpg', 'jpg', '', 1516213527, 0, 0, NULL),
(78, '/galeria/', '5a5f9520e9828.jpg', 'jpg', '', 1516213536, 0, 0, NULL),
(79, '/galeria/', '5a5f96f0f1640.jpg', 'jpg', '', 1516214001, 0, 0, NULL),
(80, '/galeria/', '5a5f96f9c8ac8.jpg', 'jpg', '', 1516214009, 0, 0, NULL),
(81, '/galeria/', '5a5f98e95b157.jpg', 'jpg', '', 1516214505, 0, 0, NULL),
(82, '/galeria/', '5a5f9921e7d0b.jpg', 'jpg', '', 1516214561, 0, 0, NULL),
(83, '/galeria/', '5a5f995f102ec.jpg', 'jpg', '', 1516214623, 0, 0, NULL),
(84, '/galeria/', '5a5f999263f99.jpg', 'jpg', '', 1516214674, 0, 0, NULL),
(85, '/galeria/', '5a5f9a1ca1ec4.jpg', 'jpg', '', 1516214812, 0, 0, NULL),
(86, 'https://www.youtube.com/watch?v=Xd7dXXPmI18', 'Youtube Video Xd7dXXPmI18', NULL, NULL, 1516215364, 1, 0, NULL),
(87, '/galeria/', '5a5f9c70927c8.jpg', 'jpg', '', 1516215408, 0, 0, NULL),
(88, '/galeria/', '5a5fa0a197afd.jpg', 'jpg', '', 1516216481, 0, 0, NULL),
(89, '/galeria/', '5a5fa3f82f190.jpg', 'jpg', '', 1516217336, 0, 0, NULL),
(90, '/galeria/', '5a5fa434e013f.jpg', 'jpg', '', 1516217396, 0, 0, NULL),
(91, '/galeria/', '5a5fa47361d93.jpg', 'jpg', '', 1516217459, 0, 0, NULL),
(92, '/galeria/', '5a5fa499b60e9.jpg', 'jpg', '', 1516217497, 0, 0, NULL),
(93, '/galeria/', '5a5fa53c52cc8.jpg', 'jpg', '', 1516217660, 0, 0, NULL),
(94, '/galeria/', '5a5fa65eb0586.jpg', 'jpg', '', 1516217950, 0, 0, NULL),
(95, '/galeria/', '5a5fa6724fe52.jpg', 'jpg', '', 1516217970, 0, 0, NULL),
(96, '/galeria/', '5a5fa69ed6ac4.jpg', 'jpg', '', 1516218014, 0, 0, NULL),
(97, '/galeria/', '5a5fa6bf8e61a.jpg', 'jpg', '', 1516218047, 0, 0, NULL),
(98, '/galeria/', '5a5fa6f4865a3.jpg', 'jpg', '', 1516218100, 0, 0, NULL),
(99, '/galeria/', '5a5fa70b8d5c1.jpg', 'jpg', '', 1516218123, 0, 0, NULL),
(100, '/galeria/', '5a5fa73ee2283.jpg', 'jpg', '', 1516218174, 0, 0, NULL),
(101, '/galeria/', '5a5fa7536437e.jpg', 'jpg', '', 1516218195, 0, 0, NULL),
(102, '/galeria/', '5a5fa77b4182c.jpg', 'jpg', '', 1516218235, 0, 0, NULL),
(103, '/galeria/', '5a5fa7c018118.jpg', 'jpg', '', 1516218304, 0, 0, NULL),
(104, '/galeria/', '5a5fab591f15e.jpg', 'jpg', '', 1516219225, 0, 0, NULL),
(105, '/galeria/', '5a5fab82a6e63.jpg', 'jpg', '', 1516219266, 0, 0, NULL),
(106, '/galeria/', '5a5fac18b3b61.jpg', 'jpg', '', 1516219416, 0, 0, NULL),
(107, '/galeria/', '5a5fac4d39df3.jpg', 'jpg', '', 1516219469, 0, 0, NULL),
(108, '/galeria/', '5a5fae401f0b9.jpg', 'jpg', '', 1516219968, 0, 0, NULL),
(109, '/galeria/', '5a5fae57aaa01.jpg', 'jpg', '', 1516219991, 0, 0, NULL),
(110, '/galeria/', '5a5faea2d8007.jpg', 'jpg', '', 1516220066, 0, 0, NULL),
(111, 'https://www.youtube.com/watch?v=6RQ_AEaWFnA', 'Youtube Video 6RQ_AEaWFnA', NULL, NULL, 1516220083, 1, 0, NULL),
(112, '/galeria/', '5a5faeff0e5d1.jpg', 'jpg', '', 1516220159, 0, 0, NULL),
(113, '/galeria/', '5a5fb0331721a.jpg', 'jpg', '', 1516220467, 0, 0, NULL),
(114, '/galeria/', '5a5fb07bd0e4e.jpg', 'jpg', '', 1516220539, 0, 0, NULL),
(115, '/galeria/', '5a664b0d1bd22.jpg', 'jpg', '', 1516653325, 0, 0, NULL),
(116, '/galeria/', '5a665019adb3e.jpg', 'jpg', '', 1516654617, 0, 0, NULL),
(117, '/galeria/', '5a665132b88da.jpg', 'jpg', '', 1516654898, 0, 0, NULL),
(118, 'https://www.youtube.com/watch?v=2_G2_WdxpU0', 'Youtube Video 2_G2_WdxpU0', NULL, NULL, 1517236984, 1, 0, NULL),
(119, 'https://www.youtube.com/watch?v=OxgSelRtRPo', 'Youtube Video OxgSelRtRPo', NULL, NULL, 1517244230, 1, 0, NULL),
(120, 'https://www.youtube.com/watch?v=OxgSelRtRPo', 'Youtube Video OxgSelRtRPo', NULL, NULL, 1517244371, 1, 0, NULL),
(121, 'https://www.youtube.com/watch?v=BTo-m27wGwg', 'Youtube Video BTo-m27wGwg', NULL, NULL, 1517244878, 1, 0, NULL),
(122, 'https://www.youtube.com/watch?v=FSWL6deTLUs', 'Youtube Video FSWL6deTLUs', NULL, NULL, 1517409967, 1, 0, NULL),
(123, '/galeria/', '5a71e32061ba5.jpg', 'jpg', '', 1517413152, 0, 0, 'file'),
(124, 'https://www.youtube.com/watch?v=2_G2_WdxpU0', 'Youtube Video 2_G2_WdxpU0', '2_G2_WdxpU0', NULL, 1517413178, 1, 0, 'youtube'),
(125, '/galeria/', '5a71e45a7bf69.jpg', 'jpg', '', 1517413466, 0, 0, 'file'),
(126, 'https://www.youtube.com/watch?v=2_G2_WdxpU0', 'Youtube Video 2_G2_WdxpU0', '2_G2_WdxpU0', NULL, 1517413472, 1, 0, 'youtube');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '5', 1499875761),
('root', '1', 1499866068);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1499875121, 1499875121),
('/banners-zonas/*', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners-zonas/create', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners-zonas/delete', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners-zonas/delete-multiple', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners-zonas/index', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners-zonas/update', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners-zonas/view', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/*', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/create', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/delete', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/delete-multiple', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/index', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/update', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/banners/view', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/consultas/*', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/consultas/create', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/consultas/delete', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/consultas/delete-multiple', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/consultas/index', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/consultas/update', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/consultas/view', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/datecontrol/*', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/datecontrol/parse/*', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/datecontrol/parse/convert', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/*', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/default/*', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/default/db-explain', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/default/download-mail', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/default/index', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/default/toolbar', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/debug/default/view', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/gii/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gii/default/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gii/default/action', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gii/default/diff', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gii/default/index', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gii/default/preview', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gii/default/view', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/gridview/*', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/gridview/export/*', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/gridview/export/download', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/imagenes/index', 2, NULL, NULL, NULL, 1507661221, 1507661221),
('/mensajes/*', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/mensajes/create', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/mensajes/delete', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/mensajes/delete-multiple', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/mensajes/index', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/mensajes/update', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/mensajes/view', 2, NULL, NULL, NULL, 1489507006, 1489507006),
('/noticias/*', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/noticias/create', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/noticias/delete', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/noticias/delete-multiple', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/noticias/index', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/noticias/update', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/noticias/view', 2, NULL, NULL, NULL, 1507323048, 1507323048),
('/notificaciones/*', 2, NULL, NULL, NULL, 1489450440, 1489450440),
('/notificaciones/create', 2, NULL, NULL, NULL, 1489450440, 1489450440),
('/notificaciones/delete', 2, NULL, NULL, NULL, 1489450440, 1489450440),
('/notificaciones/delete-multiple', 2, NULL, NULL, NULL, 1489450440, 1489450440),
('/notificaciones/index', 2, NULL, NULL, NULL, 1489450440, 1489450440),
('/notificaciones/update', 2, NULL, NULL, NULL, 1489450440, 1489450440),
('/notificaciones/view', 2, NULL, NULL, NULL, 1499875111, 1499875111),
('/notificaciones/visto', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/*', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/assignment/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/assignment/assign', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/assignment/index', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/assignment/revoke', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/assignment/view', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/assignment/view-simple', 2, NULL, NULL, NULL, 1499875116, 1499875116),
('/permisos/default/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/default/index', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/item/*', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/assign', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/create', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/delete', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/index', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/remove', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/update', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/item/view', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/permisos/menu/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/menu/create', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/menu/delete', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/menu/index', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/menu/update', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/menu/view', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/*', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/permission/assign', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/create', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/delete', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/index', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/remove', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/update', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/permission/view', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/role/*', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/assign', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/create', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/delete', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/index', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/remove', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/update', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/role/view', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/route/*', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/route/assign', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/route/create', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/route/index', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/route/refresh', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/route/remove', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/rule/*', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/rule/create', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/rule/delete', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/rule/index', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/rule/update', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/rule/view', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/permisos/user/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/activate', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/change-password', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/delete', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/index', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/login', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/logout', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/request-password-reset', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/reset-password', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/signup', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/permisos/user/view', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/productos-categorias/index', 2, NULL, NULL, NULL, 1510583165, 1510583165),
('/productos/index', 2, NULL, NULL, NULL, 1510583138, 1510583138),
('/site/*', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/site/error', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/site/index', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/site/login', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/site/logout', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/site/rootsetup', 2, NULL, NULL, NULL, 1489103581, 1489103581),
('/user-messages/*', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user-messages/create', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user-messages/delete', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user-messages/delete-multiple', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user-messages/index', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user-messages/userlist', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user-messages/view', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/user/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/admin/*', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/assignments', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/block', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/confirm', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/create', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/delete', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/index', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/info', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/resend-password', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/switch', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/update', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/admin/update-profile', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/profile/*', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/profile/index', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/profile/show', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/recovery/*', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/recovery/request', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/recovery/reset', 2, NULL, NULL, NULL, 1489103579, 1489103579),
('/user/registration/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/registration/confirm', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/registration/connect', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/registration/register', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/registration/resend', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/security/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/security/auth', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/security/login', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/security/logout', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/*', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/account', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/avatar-upload', 2, NULL, NULL, NULL, 1499820113, 1499820113),
('/user/settings/confirm', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/delete', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/disconnect', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/networks', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/user/settings/profile', 2, NULL, NULL, NULL, 1489103580, 1489103580),
('/users/admin/*', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/assignments', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/block', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/confirm', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/create', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/delete', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/index', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/info', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/resend-password', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/switch', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/update', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/admin/update-profile', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/profile/*', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/profile/index', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/profile/show', 2, NULL, NULL, NULL, 1499820114, 1499820114),
('/users/recovery/*', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/recovery/request', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/recovery/reset', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/registration/*', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/registration/confirm', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/registration/connect', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/registration/register', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/registration/resend', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/security/*', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/security/auth', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/security/login', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/security/logout', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/*', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/account', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/avatar-upload', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/confirm', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/delete', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/disconnect', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/networks', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('/users/settings/profile', 2, NULL, NULL, NULL, 1499820115, 1499820115),
('admin', 1, 'Administrador', NULL, NULL, 1489161812, 1489163413),
('root', 1, 'Super Admin ', NULL, NULL, 1489092416, 1499875143),
('user', 1, 'Usuario permisos básicos', NULL, NULL, 1489446131, 1489446131);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', '/consultas/*'),
('admin', '/consultas/create'),
('admin', '/consultas/delete'),
('admin', '/consultas/delete-multiple'),
('admin', '/consultas/index'),
('admin', '/consultas/update'),
('admin', '/consultas/view'),
('admin', '/datecontrol/*'),
('admin', '/datecontrol/parse/*'),
('admin', '/datecontrol/parse/convert'),
('admin', '/mensajes/*'),
('admin', '/mensajes/create'),
('admin', '/mensajes/delete'),
('admin', '/mensajes/delete-multiple'),
('admin', '/mensajes/index'),
('admin', '/mensajes/update'),
('admin', '/mensajes/view'),
('admin', '/notificaciones/*'),
('admin', '/notificaciones/create'),
('admin', '/notificaciones/delete'),
('admin', '/notificaciones/delete-multiple'),
('admin', '/notificaciones/index'),
('admin', '/notificaciones/update'),
('admin', '/notificaciones/view'),
('admin', '/notificaciones/visto'),
('admin', '/site/*'),
('admin', '/site/error'),
('admin', '/site/index'),
('admin', '/site/login'),
('admin', '/site/logout'),
('admin', '/user-messages/*'),
('admin', '/user-messages/create'),
('admin', '/user-messages/delete'),
('admin', '/user-messages/delete-multiple'),
('admin', '/user-messages/index'),
('admin', '/user-messages/userlist'),
('admin', '/user-messages/view'),
('admin', '/user/*'),
('admin', '/user/admin/*'),
('admin', '/user/admin/assignments'),
('admin', '/user/admin/block'),
('admin', '/user/admin/confirm'),
('admin', '/user/admin/create'),
('admin', '/user/admin/delete'),
('admin', '/user/admin/index'),
('admin', '/user/admin/info'),
('admin', '/user/admin/resend-password'),
('admin', '/user/admin/switch'),
('admin', '/user/admin/update'),
('admin', '/user/admin/update-profile'),
('admin', '/user/profile/*'),
('admin', '/user/profile/index'),
('admin', '/user/profile/show'),
('admin', '/user/recovery/*'),
('admin', '/user/recovery/request'),
('admin', '/user/recovery/reset'),
('admin', '/user/registration/*'),
('admin', '/user/registration/confirm'),
('admin', '/user/registration/connect'),
('admin', '/user/registration/register'),
('admin', '/user/registration/resend'),
('admin', '/user/security/*'),
('admin', '/user/security/auth'),
('admin', '/user/security/login'),
('admin', '/user/security/logout'),
('admin', '/user/settings/*'),
('admin', '/user/settings/account'),
('admin', '/user/settings/avatar-upload'),
('admin', '/user/settings/confirm'),
('admin', '/user/settings/delete'),
('admin', '/user/settings/disconnect'),
('admin', '/user/settings/networks'),
('admin', '/user/settings/profile'),
('admin', '/users/admin/*'),
('admin', '/users/admin/assignments'),
('admin', '/users/admin/block'),
('admin', '/users/admin/confirm'),
('admin', '/users/admin/create'),
('admin', '/users/admin/delete'),
('admin', '/users/admin/index'),
('admin', '/users/admin/info'),
('admin', '/users/admin/resend-password'),
('admin', '/users/admin/switch'),
('admin', '/users/admin/update'),
('admin', '/users/admin/update-profile'),
('admin', '/users/profile/*'),
('admin', '/users/profile/index'),
('admin', '/users/profile/show'),
('admin', '/users/recovery/*'),
('admin', '/users/recovery/request'),
('admin', '/users/recovery/reset'),
('admin', '/users/registration/*'),
('admin', '/users/registration/confirm'),
('admin', '/users/registration/connect'),
('admin', '/users/registration/register'),
('admin', '/users/registration/resend'),
('admin', '/users/security/*'),
('admin', '/users/security/auth'),
('admin', '/users/security/login'),
('admin', '/users/security/logout'),
('admin', '/users/settings/*'),
('admin', '/users/settings/account'),
('admin', '/users/settings/avatar-upload'),
('admin', '/users/settings/confirm'),
('admin', '/users/settings/delete'),
('admin', '/users/settings/disconnect'),
('admin', '/users/settings/networks'),
('admin', '/users/settings/profile'),
('root', '/*'),
('root', '/banners-zonas/*'),
('root', '/banners-zonas/create'),
('root', '/banners-zonas/delete'),
('root', '/banners-zonas/delete-multiple'),
('root', '/banners-zonas/index'),
('root', '/banners-zonas/update'),
('root', '/banners-zonas/view'),
('root', '/banners/*'),
('root', '/banners/create'),
('root', '/banners/delete'),
('root', '/banners/delete-multiple'),
('root', '/banners/index'),
('root', '/banners/update'),
('root', '/banners/view'),
('root', '/consultas/*'),
('root', '/consultas/create'),
('root', '/consultas/delete'),
('root', '/consultas/delete-multiple'),
('root', '/consultas/index'),
('root', '/consultas/update'),
('root', '/consultas/view'),
('root', '/datecontrol/*'),
('root', '/datecontrol/parse/*'),
('root', '/datecontrol/parse/convert'),
('root', '/debug/*'),
('root', '/debug/default/*'),
('root', '/debug/default/db-explain'),
('root', '/debug/default/download-mail'),
('root', '/debug/default/index'),
('root', '/debug/default/toolbar'),
('root', '/debug/default/view'),
('root', '/gii/*'),
('root', '/gii/default/*'),
('root', '/gii/default/action'),
('root', '/gii/default/diff'),
('root', '/gii/default/index'),
('root', '/gii/default/preview'),
('root', '/gii/default/view'),
('root', '/gridview/*'),
('root', '/gridview/export/*'),
('root', '/gridview/export/download'),
('root', '/mensajes/*'),
('root', '/mensajes/create'),
('root', '/mensajes/delete'),
('root', '/mensajes/delete-multiple'),
('root', '/mensajes/index'),
('root', '/mensajes/update'),
('root', '/mensajes/view'),
('root', '/noticias/*'),
('root', '/noticias/create'),
('root', '/noticias/delete'),
('root', '/noticias/delete-multiple'),
('root', '/noticias/index'),
('root', '/noticias/update'),
('root', '/noticias/view'),
('root', '/notificaciones/*'),
('root', '/notificaciones/create'),
('root', '/notificaciones/delete'),
('root', '/notificaciones/delete-multiple'),
('root', '/notificaciones/index'),
('root', '/notificaciones/update'),
('root', '/notificaciones/view'),
('root', '/notificaciones/visto'),
('root', '/permisos/*'),
('root', '/permisos/assignment/*'),
('root', '/permisos/assignment/assign'),
('root', '/permisos/assignment/index'),
('root', '/permisos/assignment/revoke'),
('root', '/permisos/assignment/view'),
('root', '/permisos/assignment/view-simple'),
('root', '/permisos/default/*'),
('root', '/permisos/default/index'),
('root', '/permisos/item/*'),
('root', '/permisos/item/assign'),
('root', '/permisos/item/create'),
('root', '/permisos/item/delete'),
('root', '/permisos/item/index'),
('root', '/permisos/item/remove'),
('root', '/permisos/item/update'),
('root', '/permisos/item/view'),
('root', '/permisos/menu/*'),
('root', '/permisos/menu/create'),
('root', '/permisos/menu/delete'),
('root', '/permisos/menu/index'),
('root', '/permisos/menu/update'),
('root', '/permisos/menu/view'),
('root', '/permisos/permission/*'),
('root', '/permisos/permission/assign'),
('root', '/permisos/permission/create'),
('root', '/permisos/permission/delete'),
('root', '/permisos/permission/index'),
('root', '/permisos/permission/remove'),
('root', '/permisos/permission/update'),
('root', '/permisos/permission/view'),
('root', '/permisos/role/*'),
('root', '/permisos/role/assign'),
('root', '/permisos/role/create'),
('root', '/permisos/role/delete'),
('root', '/permisos/role/index'),
('root', '/permisos/role/remove'),
('root', '/permisos/role/update'),
('root', '/permisos/role/view'),
('root', '/permisos/route/*'),
('root', '/permisos/route/assign'),
('root', '/permisos/route/create'),
('root', '/permisos/route/index'),
('root', '/permisos/route/refresh'),
('root', '/permisos/route/remove'),
('root', '/permisos/rule/*'),
('root', '/permisos/rule/create'),
('root', '/permisos/rule/delete'),
('root', '/permisos/rule/index'),
('root', '/permisos/rule/update'),
('root', '/permisos/rule/view'),
('root', '/permisos/user/*'),
('root', '/permisos/user/activate'),
('root', '/permisos/user/change-password'),
('root', '/permisos/user/delete'),
('root', '/permisos/user/index'),
('root', '/permisos/user/login'),
('root', '/permisos/user/logout'),
('root', '/permisos/user/request-password-reset'),
('root', '/permisos/user/reset-password'),
('root', '/permisos/user/signup'),
('root', '/permisos/user/view'),
('root', '/site/*'),
('root', '/site/error'),
('root', '/site/index'),
('root', '/site/login'),
('root', '/site/logout'),
('root', '/site/rootsetup'),
('root', '/user-messages/*'),
('root', '/user-messages/create'),
('root', '/user-messages/delete'),
('root', '/user-messages/delete-multiple'),
('root', '/user-messages/index'),
('root', '/user-messages/userlist'),
('root', '/user-messages/view'),
('root', '/user/*'),
('root', '/user/admin/*'),
('root', '/user/admin/assignments'),
('root', '/user/admin/block'),
('root', '/user/admin/confirm'),
('root', '/user/admin/create'),
('root', '/user/admin/delete'),
('root', '/user/admin/index'),
('root', '/user/admin/info'),
('root', '/user/admin/resend-password'),
('root', '/user/admin/switch'),
('root', '/user/admin/update'),
('root', '/user/admin/update-profile'),
('root', '/user/profile/*'),
('root', '/user/profile/index'),
('root', '/user/profile/show'),
('root', '/user/recovery/*'),
('root', '/user/recovery/request'),
('root', '/user/recovery/reset'),
('root', '/user/registration/*'),
('root', '/user/registration/confirm'),
('root', '/user/registration/connect'),
('root', '/user/registration/register'),
('root', '/user/registration/resend'),
('root', '/user/security/*'),
('root', '/user/security/auth'),
('root', '/user/security/login'),
('root', '/user/security/logout'),
('root', '/user/settings/*'),
('root', '/user/settings/account'),
('root', '/user/settings/avatar-upload'),
('root', '/user/settings/confirm'),
('root', '/user/settings/delete'),
('root', '/user/settings/disconnect'),
('root', '/user/settings/networks'),
('root', '/user/settings/profile'),
('root', '/users/admin/*'),
('root', '/users/admin/assignments'),
('root', '/users/admin/block'),
('root', '/users/admin/confirm'),
('root', '/users/admin/create'),
('root', '/users/admin/delete'),
('root', '/users/admin/index'),
('root', '/users/admin/info'),
('root', '/users/admin/resend-password'),
('root', '/users/admin/switch'),
('root', '/users/admin/update'),
('root', '/users/admin/update-profile'),
('root', '/users/profile/*'),
('root', '/users/profile/index'),
('root', '/users/profile/show'),
('root', '/users/recovery/*'),
('root', '/users/recovery/request'),
('root', '/users/recovery/reset'),
('root', '/users/registration/*'),
('root', '/users/registration/confirm'),
('root', '/users/registration/connect'),
('root', '/users/registration/register'),
('root', '/users/registration/resend'),
('root', '/users/security/*'),
('root', '/users/security/auth'),
('root', '/users/security/login'),
('root', '/users/security/logout'),
('root', '/users/settings/*'),
('root', '/users/settings/account'),
('root', '/users/settings/avatar-upload'),
('root', '/users/settings/confirm'),
('root', '/users/settings/delete'),
('root', '/users/settings/disconnect'),
('root', '/users/settings/networks'),
('root', '/users/settings/profile'),
('root', 'admin'),
('user', '/site/error'),
('user', '/site/index'),
('user', '/site/login'),
('user', '/site/logout'),
('user', '/user/profile/*'),
('user', '/user/profile/index'),
('user', '/user/profile/show'),
('user', '/user/recovery/*'),
('user', '/user/recovery/request'),
('user', '/user/recovery/reset'),
('user', '/user/security/*'),
('user', '/user/security/auth'),
('user', '/user/security/login'),
('user', '/user/security/logout'),
('user', '/user/settings/*'),
('user', '/user/settings/account'),
('user', '/user/settings/confirm'),
('user', '/user/settings/delete'),
('user', '/user/settings/disconnect'),
('user', '/user/settings/networks'),
('user', '/user/settings/profile');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `html` text,
  `imagen` int(11) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `activo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banners`
--

INSERT INTO `banners` (`id`, `nombre`, `html`, `imagen`, `link`, `activo`) VALUES
(1, 'Banner1', 'Hola', NULL, 'asdasd', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners_x_zonas`
--

CREATE TABLE `banners_x_zonas` (
  `banners_id` int(11) NOT NULL,
  `zonas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banners_x_zonas`
--

INSERT INTO `banners_x_zonas` (`banners_id`, `zonas_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners_zonas`
--

CREATE TABLE `banners_zonas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `activo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banners_zonas`
--

INSERT INTO `banners_zonas` (`id`, `nombre`, `activo`) VALUES
(1, 'TopSideBar', 1),
(2, 'BottomSideBar', 1),
(3, 'BodyTop', 1),
(4, 'TopBanner', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultas`
--

CREATE TABLE `consultas` (
  `id` int(11) NOT NULL,
  `remitente` varchar(128) NOT NULL,
  `destinatario` varchar(128) NOT NULL,
  `asunto` varchar(255) NOT NULL,
  `cuerpo` text NOT NULL,
  `fecha_creacion` int(11) NOT NULL,
  `visto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `consultas`
--

INSERT INTO `consultas` (`id`, `remitente`, `destinatario`, `asunto`, `cuerpo`, `fecha_creacion`, `visto`) VALUES
(1, 'maxi@manifesto.com.ar', 'julian@manifesto.com.ar', 'TEST', 'TEST', 12345678, 1),
(2, 'Maximiliano(masix@asdasd.com)', 'admin@example.com', 'test', 'testtete', 1510349255, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `name`, `parent`, `route`, `order`, `data`) VALUES
(11, 'Permisos', NULL, '/permisos/default/index', 25, 'return array (\n  \'icon\' => \'fa-balance-scale\',\n);'),
(12, 'Roles', 11, '/permisos/role/index', 3, 'return array (\n  \'icon\' => \'fa-sitemap\',\n);'),
(13, 'Asignaciones', 11, '/permisos/assignment/index', 4, 'return array (\n  \'icon\' => \'fa-users\',\n);'),
(14, 'Menu', 11, '/permisos/menu/index', 26, 'return array (\n  \'icon\' => \'fa-th-list\',\n);'),
(15, 'Permisos', 11, '/permisos/permission/index', 2, 'return array (\n  \'icon\' => \'fa-gavel\',\n);'),
(16, 'Rutas', 11, '/permisos/route/index', 1, 'return array (\n  \'icon\' => \'fa-bus\',\n);'),
(17, 'Usuarios', NULL, '/user/admin/index', 24, 'return array (\n  \'icon\' => \'fa-users\',\n);'),
(19, 'Home', NULL, '/site/index', 1, 'return array (\n  \'icon\' => \'fa-home\',\n);'),
(27, 'Consultas', NULL, '/consultas/index', 23, 'return array (\n  \'icon\' => \'fa-envelope-o\',\n);'),
(28, 'Noticias', NULL, '/noticias/index', 20, 'return array (\n  \'icon\' => \'fa-newspaper-o\',\n);'),
(29, 'Banners', NULL, '/banners/index', 21, 'return array (\n  \'icon\' => \'fa-picture-o\',\n);'),
(30, 'Banners Zonas', NULL, '/banners-zonas/index', 22, 'return array (\n  \'icon\' => \'fa-trello\',\n);'),
(31, 'Galería', NULL, '/imagenes/index', 23, 'return array (\n  \'icon\' => \'fa-file-image-o\',\n);'),
(32, 'Productos', NULL, '/productos/index', 18, 'return array (\n  \'icon\' => \'fa-cube\',\n);'),
(33, 'Productos Categorias', NULL, '/productos-categorias/index', 19, 'return array (\n  \'icon\' => \'fa-cubes\',\n);');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1489086509),
('m130524_201442_init', 1489086513),
('m140209_132017_init', 1489086558),
('m140403_174025_create_account_table', 1489086559),
('m140504_113157_update_tables', 1489086562),
('m140504_130429_create_token_table', 1489086563),
('m140506_102106_rbac_init', 1489091830),
('m140830_171933_fix_ip_field', 1489086564),
('m140830_172703_change_account_table_name', 1489086564),
('m141222_110026_update_ip_field', 1489086565),
('m141222_135246_alter_username_length', 1489086566),
('m150614_103145_update_social_account_table', 1489086568),
('m150623_212711_fix_username_notnull', 1489086568),
('m151218_234654_add_timezone_to_profile', 1489086569),
('m160929_103127_add_last_login_at_to_user_table', 1489086569);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `copete` varchar(255) NOT NULL,
  `cuerpo` text NOT NULL,
  `fecha_creacion` int(11) DEFAULT NULL,
  `fecha_modificacion` int(11) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `destacado` tinyint(4) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `vistas` int(11) NOT NULL DEFAULT '0',
  `thumb` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias_multimedia`
--

CREATE TABLE `noticias_multimedia` (
  `noticia_id` int(11) NOT NULL,
  `archivos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(96) NOT NULL,
  `descripcion` text,
  `activo` tinyint(4) NOT NULL,
  `imagen` int(11) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `fecha_creacion` int(11) DEFAULT NULL,
  `fecha_modificacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `activo`, `imagen`, `keywords`, `orden`, `fecha_creacion`, `fecha_modificacion`) VALUES
(1, 'Test Name', 'test desc', 1, 12, 'a,b', 6, NULL, 1510348727),
(3, 'MundoD', 'asdasdasdas', 1, NULL, 'a,b,c,d', 2, 1510941844, 1510941844),
(4, 'asdasd', 'asdsadsad', 0, NULL, 'a', 2, 1511535788, 1511535788),
(5, 'Test Prodcutos', 'asdasdasdsa', 1, 117, 'a,b,c,d', 2, 1517237938, 1517240233),
(6, 'Test', 'test', 1, 123, 'a,b,c', 2, 1517413192, 1517413417);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_categorias`
--

CREATE TABLE `productos_categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(96) NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_multimedia`
--

CREATE TABLE `productos_multimedia` (
  `producto_id` int(11) NOT NULL,
  `archivos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos_multimedia`
--

INSERT INTO `productos_multimedia` (`producto_id`, `archivos_id`) VALUES
(5, 117),
(6, 101),
(6, 117),
(6, 124);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_x_categorias`
--

CREATE TABLE `productos_x_categorias` (
  `producto_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `location`, `website`, `bio`, `timezone`, `avatar`) VALUES
(1, 'Manifesto Super Admin', 'maxizrnr@gmail.com', '', '', '', 'Pacific/Apia', NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(1, 'vk6JtNh6HMINC4H5JfV-KOiqo4WSnX9I', 1489091929, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
(1, 'root', 'dev@manifesto.com.ar', '$2y$10$HJnmzLnO3x8j/35CrruohOozAjXitXhZqMePk5RpfwMZixfKsLOQm', 'Z7ntr21RcrDnvBf42QidmwBBBcwV7yDT', 1489091986, NULL, NULL, '::1', 1489091929, 1489435899, 0, 1522878399),
(5, 'admin', 'admin@manifesto.com.ar', '$2y$10$z92Zxn6nEUGZNrYDTeSe6u5WLmBstgKpXoecmE5uYUr3rdhGtSRDC', 'pXyfmIBxlqOVltW8k9DFMmyUAlVN7AmM', 1499875751, NULL, NULL, '::1', 1499875751, 1507218012, 0, 1510928924),
(6, 'maxiz22', 'maxi@manifesto.com.ar', '$2y$10$UxmTQrsr50hJka4rd1vwoOU8I9UosBtqIfJ4I6RxGL4DkDMuJMxiq', 'qRa8sNXuHBz6x6vX1mKJ6pBAPkJb1LjJ', 1510950017, NULL, NULL, '::1', 1510950016, 1510950016, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_messages`
--

CREATE TABLE `user_messages` (
  `id` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `message` text NOT NULL,
  `fecha` int(11) NOT NULL,
  `visto` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_messages`
--

INSERT INTO `user_messages` (`id`, `user_from`, `user_to`, `message`, `fecha`, `visto`) VALUES
(70, 1, 1, 'asdsadasdas', 1499725186, 1),
(71, 1, 1, 'Test mensaje', 1510161621, 1),
(72, 5, 1, 'Hola locon', 1510928943, 1),
(73, 1, 5, 'Hola', 1510929133, 1),
(74, 1, 5, 'Hola viejas', 1510932376, 1),
(75, 1, 5, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 1510934044, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `fecha` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `visto` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `title`, `descripcion`, `url`, `fecha`, `type`, `visto`) VALUES
(3, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510778521, 'info', 1),
(4, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510783507, 'info', 1),
(5, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510783513, 'info', 1),
(6, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510783525, 'info', 1),
(7, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510842471, 'info', 1),
(8, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510851907, 'info', 1),
(9, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510854482, 'info', 1),
(10, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510858236, 'info', 1),
(11, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510866304, 'info', 1),
(12, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510924671, 'info', 1),
(13, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510925974, 'info', 1),
(14, 1, 'Mono', 'con navaja', '/proyecto-template/backend/index', 1510925979, 'info', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indices de la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indices de la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indices de la tabla `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagen` (`imagen`);

--
-- Indices de la tabla `banners_x_zonas`
--
ALTER TABLE `banners_x_zonas`
  ADD PRIMARY KEY (`banners_id`,`zonas_id`),
  ADD KEY `fk_bannersxzonas_zonas` (`zonas_id`);

--
-- Indices de la tabla `banners_zonas`
--
ALTER TABLE `banners_zonas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `consultas`
--
ALTER TABLE `consultas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thumb` (`thumb`);

--
-- Indices de la tabla `noticias_multimedia`
--
ALTER TABLE `noticias_multimedia`
  ADD PRIMARY KEY (`noticia_id`,`archivos_id`),
  ADD KEY `fk_noticiasarchivos_archivo` (`archivos_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thumbnail` (`imagen`);

--
-- Indices de la tabla `productos_categorias`
--
ALTER TABLE `productos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos_multimedia`
--
ALTER TABLE `productos_multimedia`
  ADD PRIMARY KEY (`producto_id`,`archivos_id`),
  ADD KEY `fk_productosmultimedia_archivos` (`archivos_id`);

--
-- Indices de la tabla `productos_x_categorias`
--
ALTER TABLE `productos_x_categorias`
  ADD PRIMARY KEY (`producto_id`,`categoria_id`),
  ADD KEY `fk_productosxcategoria_categoria` (`categoria_id`);

--
-- Indices de la tabla `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `fk_profile_archivos` (`avatar`);

--
-- Indices de la tabla `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Indices de la tabla `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- Indices de la tabla `user_messages`
--
ALTER TABLE `user_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_from` (`user_from`),
  ADD KEY `user_to` (`user_to`);

--
-- Indices de la tabla `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivos`
--
ALTER TABLE `archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `banners_zonas`
--
ALTER TABLE `banners_zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `consultas`
--
ALTER TABLE `consultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `productos_categorias`
--
ALTER TABLE `productos_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `user_messages`
--
ALTER TABLE `user_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `banners_x_zonas`
--
ALTER TABLE `banners_x_zonas`
  ADD CONSTRAINT `fk_bannersxzonas_banners` FOREIGN KEY (`banners_id`) REFERENCES `banners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_bannersxzonas_zonas` FOREIGN KEY (`zonas_id`) REFERENCES `banners_zonas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `fk_noticias_archivos_thumb` FOREIGN KEY (`thumb`) REFERENCES `archivos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias_multimedia`
--
ALTER TABLE `noticias_multimedia`
  ADD CONSTRAINT `fk_noticiasarchivos_archivo` FOREIGN KEY (`archivos_id`) REFERENCES `archivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_noticiasarchivos_noticias` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_imagen` FOREIGN KEY (`imagen`) REFERENCES `archivos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos_multimedia`
--
ALTER TABLE `productos_multimedia`
  ADD CONSTRAINT `fk_productosmultimedia_archivos` FOREIGN KEY (`archivos_id`) REFERENCES `archivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_productosmultimedia_productos` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos_x_categorias`
--
ALTER TABLE `productos_x_categorias`
  ADD CONSTRAINT `fk_productosxcategoria_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `productos_categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_productosxcategoria_producto` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_profile_archivos` FOREIGN KEY (`avatar`) REFERENCES `archivos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_messages`
--
ALTER TABLE `user_messages`
  ADD CONSTRAINT `fk_usermessage_userfrom` FOREIGN KEY (`user_from`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usermessage_userto` FOREIGN KEY (`user_to`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD CONSTRAINT `fk_usernotify_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
