<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\AlertSwal;
use yii\helpers\Url;

AppAsset::register($this);



if (empty(Yii::$app->params['title-seo']))
    \Yii::$app->params['title-seo'] = 'Titulo seo por defecto';

if (empty(Yii::$app->params['description-seo']))
    \Yii::$app->params['description-seo'] = '¡Descripción seo por defecto!.';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>

        <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@web/') . 'favicons/apple-touch-icon.png' ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web/') . 'favicons/favicon-32x32.png' ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@web/') . 'favicons/favicon-16x16.png' ?>">
        <link rel="manifest" href="<?= Yii::getAlias('@web/') . 'favicons/manifest.json' ?>">
        <meta name="theme-color" content="#DFC08D">

        <meta name="description" content="<?= \Yii::$app->params['description-seo'] ?>"/>
        <meta name="keywords" content="cemento, hormigon, concreto, cemento precio, bolsa de cemento precio, materiales de construccion baratos, cemento holcim"/>
        <meta property="og:locale" content="es_ES" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?= Html::encode(\Yii::$app->params['title-seo']) ?>" />
        <meta property="og:description" content="<?= \Yii::$app->params['description-seo'] ?>" />
        <meta property="og:url" content="<?= Url::current([], true) ?>" />
        <meta property="og:site_name" content="<?= Html::encode(\Yii::$app->params['title-seo']) ?>" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:description" content="<?= \Yii::$app->params['description-seo'] ?>" />
        <meta name="twitter:title" content="<?= Html::encode(\Yii::$app->params['title-seo']) ?>" />

    </head>
    <body>
<?php $this->beginBody() ?>

        <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => 'Manifesto',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Noticias', 'url' => ['/notas/index']],
                ['label' => 'Productos', 'url' => ['/productos/index']],
                ['label' => 'Contacto', 'url' => ['/site/contacto']],
        ];
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Registro', 'url' => ['/user/register']];
            $menuItems[] = ['label' => 'Ingresar', 'url' => ['/user/login']];
        } else {
            $menuItems[] = '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
        ?>



            <div class="container">
<?=
Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])
?>
                <?= AlertSwal::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Manifesto <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

<?php $this->endBody() ?>


        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=GA_TRACKING_ID"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments)
            }
            ;
            gtag('js', new Date());
            //GA_TRACKING_ID PARAM!
            gtag('config', 'GA_TRACKING_ID');
        </script>



    </body>
</html>
<?php $this->endPage() ?>
