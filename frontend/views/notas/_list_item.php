<?php

use yii\helpers\Url;
use common\utils\Utils;

if (is_null($model))
    return '';

if (!isset($showlink))
    $showlink = true;
?>

<div class="list-item-noticia">

    <a href="<?= Url::toRoute(['/notas/detalle', 'id' => $model->id, 'slug' => Utils::slugify($model->titulo)]) ?>">
       <img src="<?= $model->thumb0->getUrl() ?>" alt="..." class="img-background">
    </a>

    <h5><a href="<?= Url::toRoute(['/notas/detalle', 'id' => $model->id, 'slug' => Utils::slugify($model->titulo)]) ?>" > <?= $model->titulo ?> </a></h5>
    <p><?= $model->copete ?></p>
    <?php if ($showlink): ?>
        <a href="<?= Url::toRoute(['/notas/detalle', 'id' => $model->id, 'slug' => Utils::slugify($model->titulo)]) ?>" class="btn btn-primary">VER NOTA</a>

<?php endif; ?>

</div><!-- /.list-item-inner -->