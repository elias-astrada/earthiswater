<?php
/* @var $this yii\web\View */
$this->title = 'Notas';
Yii::$app->view->params['title-seo'] = 'Noticias';
//Yii::$app->view->params['description-seo'] 
?>



<div class="listado">
    <h1>Notas</h1>
    <form action="?" method="get">
        <label for="q" class="hidden">Search</label>

        <input type="search" name="q" id="q" value="<?= $search ?>" placeholder="Buscar" class="search__field">

        <button type="submit" class="search__btn">
           Buscar
        </button>
    </form> 

    <?= $this->render('_list_notas', compact('dataProvider')); ?>

</div>




