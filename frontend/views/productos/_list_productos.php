<?php

use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * https://github.com/kop/yii2-scroll-pager
 * ListView::widget([
  'dataProvider' => $dataProvider,
  'itemOptions' => ['class' => 'item'],
  'itemView' => '_item_view',
  'pager' => ['class' => \kop\y2sp\ScrollPager::className()]
  ]); */
?>

<?=

ListView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'list-productos',
    //'layout' => '<div class="items">{items}</div>{pager}',
    'layout' => '<div class="items">{items}</div><div class="list-productos-actions">{pager}</div>',
    'itemOptions' => ['tag' => 'div', 'class' => 'col-lg-4 col-md-4 col-sm-6 col-xs-12 list-item list-productos-item'],
    'options' => [
	'tag' => 'div',
	'class' => 'list-items list-productos',
    ],
    'itemView' => '_list_productos_item',
    'pager' => [
	'class' => InfiniteScrollPager::className(),
	'widgetId' => 'list-productos',
	'itemsCssClass' => 'items',
	'nextPageLabel' => 'Cargar más Notas',
        'contentLoadedCallback' => 'function (data) { if(data.length < ' . $dataProvider->pagination->pageSize . ') $(".list-productos-actions").hide(); }',
	'linkOptions' => [
	    'class' => 'btn btn-primary',
	],
	'pluginOptions' => [
	    'loading' => [
		'msgText' => "<em>Cargando...</em>",
		'finishedMsg' => "<em>No se han encontrado mas resultados</em>",
	    ],
	    'behavior' => InfiniteScrollPager::BEHAVIOR_TWITTER,
	],
    ],
]);
?>


