<?php

use yii\helpers\Url;
use common\utils\Utils;

if (is_null($model))
    return '';

if (!isset($showlink))
    $showlink = true;
?>




<div class="list-productos-producto">

    <a href="<?= Url::toRoute(['/productos/detalle', 'id' => $model->id, 'slug' => Utils::slugify($model->nombre)]) ?>">
        <img src="<?= $model->imagen0->getUrl("16/9x1") ?>"  alt="..." class="img-background">
    </a>
    <div class="list-productos-producto-categorias">
        <?php foreach ($model->categorias as $categoria): ?>
            <a href="#" class="categoria-item"><?= $categoria->nombre ?></a>
        <?php endforeach; ?>
    </div>

    <h3><a href="<?= Url::toRoute(['/productos/detalle', 'id' => $model->id, 'slug' => Utils::slugify($model->nombre)]) ?>" > <?= $model->nombre ?> </a></h3>

    <p class="list-productos-productos-desc"><?= Utils::limit_text($model->descripcion, 20) ?> </p>
    
    
    
    <?php if ($showlink): ?>
        <a href="<?= Url::toRoute(['/productos/detalle', 'id' => $model->id, 'slug' => Utils::slugify($model->nombre)]) ?>" class="btn btn-primary">VER DETALLE</a>

    <?php endif; ?>

</div><!-- /.list-item-inner -->