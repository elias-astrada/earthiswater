<?php
$webroot = dirname(__DIR__) . '/www';
$web = rtrim(dirname($_SERVER["SCRIPT_NAME"]), '/');

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => $params['webname'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
        'uploadsUrl' => $web . '/uploads',
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => false,
            'enableRegistration' => true,
            'enablePasswordRecovery' => true,
            'viewPath' => '@app/views/user',
            'controllerMap' => [
                'admin' => 'frontend\controllers\users\AdminController',
                'profile' => 'frontend\controllers\users\ProfileController',
                'recovery' => 'frontend\controllers\users\RecoveryController',
                'registration' => 'frontend\controllers\users\RegistrationController',
                'security' => 'frontend\controllers\users\SecurityController',
                'settings' => 'frontend\controllers\users\SettingsController',
            ],
            'modelMap' => [
                'Profile' => 'common\models\Profile',
                'User' => 'common\models\User',
                'UserSearch' => 'common\models\UserSearch',
                'LoginForm' => 'common\models\LoginForm',
                'RegistrationForm' => 'common\models\RegistrationForm',
                'ResendForm' => 'common\models\ResendForm',
                'SettingsForm' => 'common\models\SettingsForm',
            ],
        ],
    ],
    'components' => [
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => 'plugins/jquery/',
                    //'jsOptions' => ['position' => 1],
                    'js' => ['jquery.min.js']
                // 'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => 'plugins/bootstrap/',
                    'css' => [
                    //'css/bootstrap.min.css'
                    ],
                //'css' => [],
                //'js' => ['vendor/tether/tether.js','vendor/bootstrap/bootstrap.js',]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => 'plugins/bootstrap/',
                    'js' => [
                    //'js/bootstrap.min.js',
                    ],
                    'depends' => [
                        'yii\web\JqueryAsset',
                    //'backend\assets\TetherAsset'
                    ]
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'baseUrl' => $params['ruta_base'],
            'csrfParam' => '_csrf-frontend',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => $params['ruta_base'],
            'rules' => [
                '/notas/<id:\w+>/<slug>' => '/notas/detalle/',
                '/notas/listado/<categoria:\w+>/<slug>' => '/notas/listado/',
                '<action:\w+>' => 'site/<action>',
                'auth/<controller:\w+>/<action:\w+>' => 'auth/<controller>/<action>',
                'user/<controller:\w+>/<action:\w+>' => 'user/<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
