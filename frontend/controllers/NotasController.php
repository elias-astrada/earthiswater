<?php

namespace frontend\controllers;

use common\models\search\NoticiasSearch;
use common\models\Noticias;
use yii\web\NotFoundHttpException;
use Yii;

class NotasController extends \yii\web\Controller {

    public function actionDetalle($id) {


        $model = $this->findModel($id);

        $searchModel = new NoticiasSearch();
        $relacionados = $searchModel->searchRelacionadas($model, $id);


        $v = $model->vistas;
        $model->vistas = $v + 1;
        $model->save(false);

        Yii::$app->params['title-seo'] = $model->titulo;
        Yii::$app->params['description-seo'] = $model->copete;
        \Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $model->keywords,
        ]);


        
        return $this->render('detalle', compact('model', 'relacionados'));
    }

    public function actionIndex() {
        return $this->redirect(['notas/listado']);
    }

    public function actionListado() {


        $search = Yii::$app->request->get('q');
        
        $searchModel = new NoticiasSearch();
        $dataProvider = $searchModel->searchFront($search, null);

        return $this->render('index', compact('search', 'dataProvider'));
    }

    protected function findModel($id) {
        if (($model = Noticias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }
    
    
    /*
    public function buscarProgramador(){
        $te_buscamos = "BUSCAMOS PROGRAMADOR";
        
        return $te_buscamos . "Sumate a nuestro equipo!";
    }
    */
}
